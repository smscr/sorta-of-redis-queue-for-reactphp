<?php

namespace Shavshukov\RedisQueue\Errors;

use Throwable;
use Shavshukov\RedisQueue\Interfaces\ErrorHandlerInterface;

trait ErrorHandlingTrait
{
    
    /**
     * @var ErrorHandlerInterface|null
     */
    protected $errorHandler;
    
    /**
     * Set error handler
     * 
     * @param ErrorHandlerInterface $handler
     */
    public function setErrorHandler(ErrorHandlerInterface $handler)
    {
        $this->errorHandler = $handler;
    }
    
    /**
     * @param Throwable $error
     * @param bool $fatal
     */
    public function reportError(Throwable $error, bool $fatal)
    {
        if ($this->errorHandler) {
            return $this->errorHandler->report($error, $fatal);
        } else {
            echo $this->getErrorPrint($error);
            
            if ($fatal) {
                die;
            }
        }
    }
    
    /**
     * Get error print string
     * 
     * @param Throwable $error
     * @return string
     */
    private function getErrorPrint(Throwable $error)
    {
        $class = get_class($error);
        $previous = $error->getPrevious();
        
        $previousPrint = '';
        if ($previous) {
            try {
                $previousPrint = implode(PHP_EOL, [
                    "Previous:",
                    $this->getErrorPrint($previous)
                ]);
            } catch (\Throwable $e) {}
        }
        
        return implode(PHP_EOL, [
            "Error ({$class})",
            $error->getMessage(),
            "File: {$error->getFile()} line {$error->getLine()}",
            "Code: {$error->getCode()}",
            "Trace:",
            $error->getTraceAsString(),
            $previousPrint
        ]);
    }
    
}
