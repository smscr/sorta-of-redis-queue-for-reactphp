<?php

namespace Shavshukov\RedisQueue\Objects;

class MessageHash extends AbstactObject
{
    
    /**
     * @var string
     */
    protected $objectName = 'message';

    /**
     * @var array
     */
    protected $fields = [
        'body',
        'headers',
        'timestamp',
        'isClaimed',
        'ttlHeader',
        'queueId'
    ];
    
    /**
     * @var array [
     *     string $fieldName => string $key
     * ]
     */
    protected $fieldsKeys = [];

    
}
