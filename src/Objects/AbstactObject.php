<?php

namespace Shavshukov\RedisQueue\Objects;

use Exception;
use Generator;
use Shavshukov\RedisQueue\{
    Client\Multi,
    Client\ClientPipeline,
    Selects\HashColumn,
    Selects\HashObject,
    Queue\QueueKeys
};

abstract class AbstactObject
{

    /**
     * @var string
     */
    protected $objectName;

    /**
     * @var array
     */
    protected $fields = [];
    
    /**
     * @var array [
     *     string $fieldName => string $key
     * ]
     */
    protected $fieldsKeys = [];
    
    /**
     * Constructor
     * 
     * @param QueueKeys $keys
     */
    public function __construct(QueueKeys $keys)
    {
        foreach ($this->fields as $fieldName) {
            $this->fieldsKeys[$fieldName] = $keys->getHashField($this->objectName, $fieldName);
        }
    }
    
    /**
     * Create one column selector
     * 
     * @param ClientPipeline $client
     * @param string $fieldName
     * @return HashColumn
     * @throws Exception
     */
    public function createColumnSelector(ClientPipeline $client, string $fieldName): HashColumn
    {
        if (!isset($this->fieldsKeys[$fieldName])) {
            throw new Exception("Unknown field: {$fieldName}");
        }

        return new HashColumn($client, $this->fieldsKeys[$fieldName]);
    }
    
    /**
     * Create one column selector
     * 
     * @param ClientPipeline $client
     * @param array $fieldNames
     * @return HashObject
     * @throws Exception
     */
    public function createObjectSelector(ClientPipeline $client, array $fieldNames): HashObject
    {
        if (!$fieldNames) {
            throw new Exception('At least one field must be specified');
        }
        
        $diff = \array_diff($fieldNames, $this->fields);
        
        if ($diff) {
            $implodeDiff = \implode(', ', $diff);
            throw new Exception("Unknown fields: {$implodeDiff}");
        }
        
        $hashKeys = [];
        foreach ($fieldNames as $fieldName) {
            $hashKeys[$fieldName] = $this->fieldsKeys[$fieldName];
        }

        return new HashObject($client, $hashKeys);
    }
    
    /**
     * Update (in bulk)
     * 
     * @param Multi $multi
     * @param array $values [
     *     string $fieldName => array [...$fieldValues],
     * ]
     * @return Generator|null
     * @throws Exception
     */
    public function bulkUpdate(Multi $multi, array $values)
    {
        foreach ($values as $fieldName => $fieldValues) {
            if (!isset($this->fieldsKeys[$fieldName])) {
                throw new Exception("Unknown field: {$fieldName}");
            }
            
            $yields[] = $multi->hmset($this->fieldsKeys[$fieldName], ...$fieldValues);
        }
        
        yield $yields;
    }
    
    /**
     * Delete columns values (in bulk)
     * 
     * @param Multi $multi
     * @param array $values [
     *     string $fieldName => array [...$fieldValues],
     * ]
     * @return Generator|null
     * @throws Exception
     */
    public function bulkDelete(Multi $multi, array $values)
    {
        foreach ($values as $fieldName => $fieldValues) {
            if (!isset($this->fieldsKeys[$fieldName])) {
                throw new Exception("Unknown field: {$fieldName}");
            }
            
            $yields[] = $multi->hdel($this->fieldsKeys[$fieldName], ...$fieldValues);
        }
        
        yield $yields;
    }
    
    /**
     * Update (entry by key)
     * 
     * @param Multi $multi
     * @param array $entriesByKey [
     *     string $key => array [
     *         string $fieldName => mixed $value,
     *         ...
     *     ],
     *     ...
     * ]
     * @return Generator|null
     * @throws Exception
     */
    public function update(Multi $multi, array $entriesByKey)
    {
        $yields = [];
        foreach ($this->fields as $fieldName) {
            $values = [];
            foreach ($entriesByKey as $key => $entry) {
                if (isset($entry[$fieldName])) {
                    $values[] = $key;
                    $values[] = $entry[$fieldName];
                }
            }
            
            $yields[] = $multi->hmset($this->fieldsKeys[$fieldName], ...$values);
        }
        
        yield $yields;
    }
    
    /**
     * Completely delete all hashes for entries
     * 
     * @param Multi $multi
     * @param string[] $entriesKeys
     * @return Generator|null
     * @throws Exception
     */
    public function deleteEntries(Multi $multi, array $entriesKeys)
    {
        $yields = [];
        foreach ($this->fields as $fieldName) {
            $yields[] = $multi->hdel($this->fieldsKeys[$fieldName], ...$entriesKeys);
        }
        
        yield $yields;
    }
    
    /**
     * Insert (entry by key)
     * 
     * @param Multi $multi
     * @param array $entriesByKey [
     *     string $key => array [
     *         string $fieldName => mixed $value,
     *         ...
     *     ],
     *     ...
     * ]
     * @return Generator|null
     * @throws Exception
     */
    public function insert(Multi $multi, array $entriesByKey)
    {
        $yields = [];
        foreach ($this->fields as $fieldName) {
            $values = [];
            foreach ($entriesByKey as $key => $entry) {
                $values[] = $key;
                $values[] = $entry[$fieldName] ?? '';
            }
            
            $yields[] = $multi->hmset($this->fieldsKeys[$fieldName], ...$values);
        }
        
        yield $yields;
    }    
}
