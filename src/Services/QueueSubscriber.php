<?php

namespace Shavshukov\RedisQueue\Services;

use Generator;
use Throwable;

use React\Promise\Deferred;
use React\Promise\PromiseInterface;
use function Shavshukov\Coroutine\coroutine;

use Shavshukov\RedisQueue\{
    Client\Multi,
    Client\Connection,
    Queue\QueueInfo,
    Queue\QueueInfoList,
    Queue\QueueKeys,
    Selects\HashColumn,
    Selects\HashObject,
    Objects\MessageHash,
    Services\CountersService,
    Interfaces\SubscriptionInterface,
    Interfaces\MessageFactoryInterface,
    Interfaces\RejectableMessageInterface
};

class QueueSubscriber extends AtomicResident
{
    use DeadLetteringTrait;
    
    const DEAD_LETTER_REASON_REJECTED = 'rejected';
    const DEAD_LETTER_REASON_EXPIRED = 'expired';
    const DEAD_LETTER_REASON_MAXLEN = 'maxlen';
    
    const BULK_SIZE = 10;
    const HEARTBEAT_RATE_REPORT_ALIVE = 4.9;
    const SUBSCRIBER_TIMEOUT = 10.0;
    
    /**
     * @var string|null
     */
    protected $id;
    
    /**
     * @var string|null
     */
    protected $queueId;
    
    /**
     * @var string|null
     */
    protected $queueName;
    
    /**
     * @var QueueInfo
     */
    protected $queueInfo;
    
    /**
     * @var QueueInfoList
     */
    protected $queueInfoList;

    /**
     * @var QueueInfo|null
     */
    protected $dlxQueueInfo;

    /**
     * @var string|null
     */
    protected $dealerId;
    
    /**
     * @var string
     */
    protected $consumerPickupKey;
    
    /**
     * @var string
     */
    protected $bucketKey;
    
    /**
     * @var string
     */
    protected $channelKey;
    
    /**
     * @var QueueKeys
     */
    protected $keys;
    
    /**
     * @var MessageHash
     */
    protected $messageHash;
    
    /**
     * @var HashColumn
     */
    protected $messageHeadersSelector;
    
    /**
     * @var HashColumn
     */
    protected $messageTimestampSelector;
    
    /**
     * @var HashColumn
     */
    protected $isClaimedSelect;
    
    /**
     * @var HashObject
     */
    protected $contentSelect;
    
    /**
     * @var MessageFactoryInterface
     */
    protected $messageFactory;
    
    /**
     * @var string
     */
    protected $prefix;
    
    /**
     * @var SubscriptionInterface
     */
    protected $subscription;
    
    /**
     * @var bool
     */
    protected $isChecking = false;
    
    /**
     * @var PromiseInterface|null
     */
    protected $processPromise = null;
    
    /**
     * Constructor
     * 
     * @param Connection $connection
     * @param SubscriptionInterface $subscription
     * @param QueueInfo $queueInfo
     */
    public function __construct(Connection $connection, SubscriptionInterface $subscription, QueueInfo $queueInfo)
    {
        parent::__construct($connection);
        
        $this->subscription = $subscription;
        $this->messageFactory = $subscription->getMessageFactory();
        
        $this->keys = $connection->getQueueKeys();
        $this->messageHash = $this->keys->message;
        $this->messageHeadersSelector = $this->messageHash->createColumnSelector($this->client, 'headers');
        $this->messageTimestampSelector = $this->messageHash->createColumnSelector($this->client, 'timestamp');
        $this->isClaimedSelect = $this->messageHash->createColumnSelector($this->client, 'isClaimed');
        $this->contentSelect = $this->messageHash->createObjectSelector($this->client, ['body', 'headers']);
        $this->prefix = $connection->getPrefix();
        $this->queueId = $queueInfo->getId();
        $this->queueName = $queueInfo->getName();
        $this->queueInfo = $queueInfo;
        $this->queueInfoList = $connection->getQueueInfoList();
        
        $hexId = \bin2hex($this->queueId);
        $this->bucketKey = "{$this->prefix}.zSets.queue.{$hexId}";
        $this->channelKey = "{$this->prefix}.channel.queue.{$hexId}";
    }
    
    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @inheritdoc
     */
    protected function getServiceName(): string
    {
        return 'QueueSubscriber';
    }

    /**
     * @inheritdoc
     */
    protected function register()
    {
        $this->id = yield $this->connection->generateUniqueId(CountersService::TYPE_SUBSCRIBER);
        $this->consumerPickupKey = $this->createConsumerPickupKey($this->id);
        $dlxQueueName = $this->queueInfo->getDlxQueueName();
        
        if ($dlxQueueName) {
            $this->dlxQueueInfo = yield $this->queueInfoList->getQueueInfoByName($dlxQueueName);
        }
        
        yield $this->reportAlive();
    }

    /**
     * @inheritdoc
     */
    protected function checkRegistration()
    {
        yield $this->client->subscribe($this->keys->channelDealerName);
        $this->dealerId = yield $this->client->get($this->keys->currentDealerIdKey);
        if (!$this->dealerId) {
            echo "QueueDealer is not found!\n";
            return false;
        }
        
        echo "Subscribe: {$this->channelKey}\n";
        yield $this->client->subscribe($this->channelKey);
        yield $this->checkQueue();
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function collectOldResidents()
    {
        return;
    }

    /**
     * @inheritdoc
     */
    protected function createTimers(): array
    {
        return [
            [self::HEARTBEAT_RATE_REPORT_ALIVE, function () {
                yield [
                    $this->reportAlive(),
                ];
            }]
        ];
    }
    
    /**
     * @inheritdoc
     */
    protected function createHandlers(): array
    {
        return [
            'message' => [
                function ($channel, $payload) {
                    // echo "Something!\n";
                    if ($channel === $this->keys->channelDealerName && $payload !== $this->dealerId) {
                        $this->dealerId = $payload;
                    } else if ($channel === $this->channelKey) {
                        $result = yield $this->checkQueue();
                        return $result;
                    }
                }
            ]
        ];
    }

    /**
     * Get name of the control key used by QueueSubscriber
     * 
     * @param string $id
     * @return string
     */
    protected function createConsumerPickupKey(string $id): string
    {
        $hexId = \bin2hex($id);
        return "{$this->prefix}.zSets.consumerPickup.{$hexId}";
    }
    
    /**
     * Claim messages and call their processing
     * 
     * @return PromiseInterface
     */
    protected function checkQueue()
    {
        if ($this->state === self::STATE_ENDED) {
            // echo "State was ended...\n";
            return;
        }
        
        if ($this->isChecking) {
            // echo "Was checking already...\n";
            return;
        }
        
        $this->isChecking = true;
        return coroutine(function() {
            // get the oldest message of due to process set
            $bulkResult = yield $this->client->zrangebyscore($this->bucketKey, '-inf', \microtime(true), 'WITHSCORES', 'LIMIT', 0, self::BULK_SIZE);

            if (!$bulkResult) {
                return;
            }

            $timestamps = [];
            $messageIds = [];
            $claimingMessage = [];
            foreach ($bulkResult as $index => $value) {
                if ($index % 2 === 0) {
                    $messageIds[] = $value;
                    $claimingMessage[] = $value;
                    $claimingMessage[] = 1;
                    continue;
                }

                $timestamps[] = $value;
                $nextValue = $bulkResult[$index - 1];
                $bulkResult[$index - 1] = $value;
                $bulkResult[$index] = $nextValue;
            }

            yield $this->claimMessages($messageIds, $timestamps, $bulkResult, $claimingMessage);

            if ($messageIds) {
                yield [
                    $this->client->zadd($this->consumerPickupKey, ...$bulkResult),
                    $this->processMessages(\array_values($messageIds))
                ];
            }

            $this->loop->futureTick(function () {
                return $this->checkQueue();
            });
        })->otherwise(function ($error) {
            $this->connection->reportError($error, false);
        })->done(function () {
            $this->isChecking = false;
            // echo "Done called\n";
        });
    }
    
    /**
     * @param array $messageIds
     * @param array $timestamps
     * @param array $bulkResult
     * @param array $claimingMessage
     * @return Generator|array
     */
    protected function claimMessages(array &$messageIds, array &$timestamps, array &$bulkResult, array $claimingMessage)
    {
        // claim it
        $claimResult = yield $this->client->multi(function (Multi $multi) use ($messageIds, $claimingMessage) {
            yield $multi->zrem($this->bucketKey, ...$messageIds);
            yield $multi->zrem($this->keys->ttlTimeoutsKey, ...$messageIds);
            yield $this->isClaimedSelect->pluck($messageIds, $multi);
            yield $this->messageHash->bulkUpdate($multi, ['isClaimed' => $claimingMessage]);
        });

        // filter out foreign claims
        $notClaimedIds = $claimResult[2];
        foreach ($notClaimedIds as $index => $wasClaimedByOther) {
            if ($wasClaimedByOther) {
                $notClaimedIds[$index] = $messageIds[$index];
                unset($messageIds[$index], $timestamps[$index], $bulkResult[2 * $index], $bulkResult[2 * $index + 1]);
            } else {
                unset($notClaimedIds[$index]);
            }
        }
        
        return $notClaimedIds;
    }
    
    /**
     * @return Generator|null
     */
    protected function reportAlive()
    {
        $timeoutTimestamp = \microtime(true) + self::SUBSCRIBER_TIMEOUT;
        yield $this->client->zadd($this->keys->consumerTimeoutsKey, $timeoutTimestamp, $this->id);
    }
    
    /**
     * Process messages
     * 
     * @param array $messageIds
     * @return type
     */
    protected function processMessages(array $messageIds)
    {
        if ($this->state === self::STATE_ENDED) {
            // echo "State was ended...\n";
            return;
        }

        $result = yield $this->contentSelect->columns($messageIds);
        $headers = $result['headers'];
        $bodies = $result['body'];
        $toDlxIds = [];
        
        foreach ($messageIds as $i => $messageId) {
            if ($this->state === self::STATE_ENDED) {
                return;
            }

            $messageHeaders = $headers[$i] ? \json_decode($headers[$i], true) : [];
            $messageBody = $bodies[$i];
            $queueMessage = $this->messageFactory->createMessage($this, [
                'id' => \bin2hex($messageId),
                'body' => $messageBody,
                'headers' => $messageHeaders,
                'queueName' => $this->queueName
            ]);
            $deferred = new Deferred;
            $this->processPromise = $deferred->promise();
            
            try {
                yield $this->subscription->receive($queueMessage);
                $deferred->resolve();
            } catch (Throwable $e) {
                $this->connection->reportError($e, false);
                $toDlxIds[] = $messageId;
            }
        }
        
        if ($toDlxIds && $this->state !== self::STATE_ENDED) {
            yield $this->processDlxIds($toDlxIds);
        }
    }
    
    /**
     * Accept message (delete it)
     * 
     * @param string $messageId
     * @return PromiseInterface
     */
    public function accept(string $messageId)
    {
        $id = \hex2bin($messageId);
        return coroutine(function() use ($id) {
            yield $this->client->multi(function (Multi $multi) use ($id) {
                yield $this->deleteMessages($multi, [$id]);
            });
        })->otherwise(function ($error) {
            $this->connection->reportError($error, false);
        });
    }
    
    /**
     * Rejection of the message
     * 
     * @param RejectableMessageInterface $message
     * @param bool $requeue
     * @return PromiseInterface
     */
    public function reject(RejectableMessageInterface $message, bool $requeue = true)
    {
        $id = \hex2bin($message->getId());
        return coroutine(function() use ($id, $requeue) {
            if ($requeue) {
                yield $this->client->multi(function (Multi $multi) use ($id) {
                    yield $this->requeueMessages($multi, [$id]);
                });
            } else {
                yield $this->processDlxIds([$id]);
            }
        })->otherwise(function ($error) {
            $this->connection->reportError($error, false);
        });
    }
    
    /**
     * Process id's which need to be sent to dead-letter exchange
     * 
     * @param array $messageIds
     */
    protected function processDlxIds(array $messageIds)
    {
        $timestamps = yield $this->messageTimestampSelector->pluck($messageIds);
        
        $toDelete = [];
        $toDeadExchange = [];
        foreach ($messageIds as $i => $messageId) {
            if ($this->dlxQueueInfo) {
                $toDeadExchange['id'][] = $messageId;
                $toDeadExchange['queueInfo'][] = $this->queueInfo;
                $toDeadExchange['dlxQueueInfo'][] = $this->dlxQueueInfo;
                $toDeadExchange['originalExpiration'][] = $timestamps[$i];
            } else {
                // if we cannot send the message to any dlx queue, and cannot requeue, can only delete then
                $toDelete[] = $messageId;
            }
        }
        
        if ($toDelete) {
            yield $this->client->multi(function (Multi $multi) use ($toDelete) {
                yield $this->deleteMessages($multi, $toDelete);
            });
        }
        
        if ($toDeadExchange) {
            yield $this->processDeadLetters($toDeadExchange, self::DEAD_LETTER_REASON_REJECTED);
        }
    }
    
    /**
     * @return Generator|mixed
     */
    public function shutdown()
    {
        // wait for processing of the message, if any
        yield $this->processPromise;
        yield parent::shutdown();
    }
    
    /**
     * @param Multi $multi
     * @param string[] $id
     * @return Generator|null
     */
    protected function deleteMessages(Multi $multi, array $id)
    {
        yield $multi->zrem($this->consumerPickupKey, ...$id);
        yield $multi->zrem($this->keys->ttlTimeoutsKey, ...$id);
        yield $multi->zrem($this->keys->dueSortedSetKey, ...$id);
        yield $this->messageHash->deleteEntries($multi, $id);
    }
    
    /**
     * @param Multi $multi
     * @param string[] $id
     * @param array[float, string, ...] $timestampId
     * @return Generator|null
     */
    protected function requeueMessages(Multi $multi, array $id, array $timestampId)
    {
        yield $multi->zrem($this->consumerPickupKey, ...$id);
        yield $multi->zadd($this->keys->dueSortedSetKey, ...$timestampId);
        yield $this->messageHash->bulkDelete($multi, ['isClaimed' => $id]);
    }

}
