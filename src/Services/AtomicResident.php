<?php

namespace Shavshukov\RedisQueue\Services;

use Exception;
use Generator;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use function Shavshukov\Coroutine\coroutine;

use Shavshukov\RedisQueue\Client\{
    Connection,
    ClientPipeline
};

abstract class AtomicResident
{
    const STATE_INIT = 0;
    const STATE_STARTED = 1;
    const STATE_ENDED = 2;
    
    /**
     * @var Connection
     */
    protected $connection;
    
    /**
     * @var ClientPipeline 
     */
    protected $client;
    
    /**
     * @var LoopInterface
     */
    protected $loop;
    
    /**
     * @var string
     */
    protected $serviceName;
    
    /**
     * @var TimerInterface[]
     */
    protected $timers = [];
    
    /**
     * @var int 
     */
    protected $state = self::STATE_INIT;
    
    /**
     * @var callable[]
     */
    protected $handlers = [];
    
    /**
     * Constructor
     * 
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->serviceName = $this->getServiceName();
        
        $this->connection = $connection;
        $this->client = $connection->getClientPipeline();
        $this->loop = $connection->getLoop();
    }
    
    /**
     * @return string
     */
    abstract protected function getServiceName(): string;
    
    /**
     * Called to register the resident
     */
    abstract protected function register();

    /**
     * Check that current instance matches the registered one
     * 
     * @return Generator|null
     */
    abstract protected function checkRegistration();
    
    /**
     * Collect data left by old residents
     * 
     * @return Generator|null
     */
    abstract protected function collectOldResidents();

    /**
     * @return array
     */
    abstract protected function createTimers(): array;

    /**
     * @return array
     */
    abstract protected function createHandlers(): array;
    
    /**
     * @return Generator|null
     */
    public function init()
    {
        if ($this->state > self::STATE_INIT) {
            throw new Exception("{$this->serviceName} is already initialized");
        }
        
        $this->state = self::STATE_STARTED;
        
        echo "Starting {$this->serviceName}\n";
        yield $this->initStatus();
        yield $this->initHandlers();
        yield $this->atomicStatus();
        yield $this->initTimers();
    }
    
    /**
     * @return Generator|mixed
     */
    public function shutdown()
    {
        if ($this->state === self::STATE_ENDED) {
            return;
        }
        
        echo "Shutdown {$this->serviceName}...\n";
        
        $this->state = self::STATE_ENDED;
        $this->client->end();

        foreach ($this->timers as $timer) {
            $this->loop->cancelTimer($timer);
        }
        
        yield $this->client->removeAllListeners();
    }
    
    /**
     * Replace previous resident
     * 
     * @return Generator|null
     */
    protected function initStatus()
    {
        if ($this->state === self::STATE_ENDED) {
            throw new Exception("{$this->serviceName} is ended");
        }
        
        yield $this->register();
        echo "Init {$this->serviceName} status\n";
    }
    
    /**
     * Ensure atomicity: if in the process between creating an id and subscribing to a channel of resident id's
     * the new id was assigned, then there is no need to continue
     * 
     * @return Generator|mixed
     */
    protected function atomicStatus()
    {
        if ($this->state === self::STATE_ENDED) {
            throw new Exception("{$this->serviceName} is ended");
        }
        
        $check = yield $this->checkRegistration();
        
        if ($check) {
            yield $this->collectOldResidents();
        } else {
            yield $this->shutdown();
        }
    }
    
    /**
     * @return null
     */
    protected function initTimers()
    {
        if ($this->state === self::STATE_ENDED) {
            throw new Exception("{$this->serviceName} is ended");
        }
        
        foreach ($this->createTimers() as $timer) {
            $this->addLoopTimer(...$timer);
        }
    }
    
    /**
     * @param float $rate
     * @param callable $loop
     * @return null
     */
    protected function addLoopTimer(float $rate, callable $loop)
    {
        if ($this->state === self::STATE_ENDED) {
            return;
        }
        
        $lock = false;
        $this->timers[] = $this->loop->addPeriodicTimer($rate, function () use ($loop, &$lock) {
            if ($lock || $this->state === self::STATE_ENDED) {
                return;
            }

            $lock = true;
            coroutine($loop)->otherwise(static function ($error) {
                $this->connection->reportError($error, false);
            })->done(static function () use (&$lock) {
                $lock = false;
            });
        });
    }
    
    /**
     * @return Generator|null
     */
    protected function initHandlers()
    {
        if ($this->state === self::STATE_ENDED) {
            throw new Exception("{$this->serviceName} is ended");
        }
        
        foreach ($this->createHandlers() as $event => $listeners) {
            foreach ($listeners as $listener) {
                $handler = function (...$arguments) use ($listener) {
                    if ($this->state === self::STATE_ENDED) {
                        return;
                    }

                    return coroutine(static function () use ($listener, $arguments) {
                        $result = yield $listener(...$arguments);
                        return $result;
                    })->otherwise(static function ($error) {
                        $this->connection->reportError($error, false);
                    });
                };
                
                $this->handlers[$event][] = $handler;
                $yields[] = $this->client->on($event, $handler);
            }
        }
        
        yield $yields;
        echo "Initialized {$this->serviceName} handlers\n";
    }
    
    
}
