<?php

namespace Shavshukov\RedisQueue\Services;

use Generator;
use Shavshukov\RedisQueue\Client\Connection;

class CountersService
{
    const COUNTERS_BIN_TTL = 2;
    
    const TYPE_QUEUE = 'queue';
    const TYPE_DEALER = 'dealer';
    const TYPE_MESSAGE = 'message';
    const TYPE_SUBSCRIBER = 'subscriber';
    
    /**
     * ClientPipeline instance
     *
     * @var ClientPipeline
     */
    protected $client;
    
    /**
     * Prefix for all key names
     *
     * @var string
     */
    protected $prefix;
    
    /**
     * Constructor
     * 
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->client = $connection->getClientPipeline();
        $this->prefix = $connection->getPrefix();
    }
    
    /**
     * Generate an entity id with a very strong uniqueness
     * 
     * @param string $type
     * @return Generator|string
     */
    public function generateUniqueId(string $type)
    {
        $time = time();
        $keyName = "{$this->prefix}.counters.{$type}.{$time}";
        
        $result = yield $this->client->multi(function ($multi) use ($keyName) {
            yield $multi->incr($keyName);
            yield $multi->expire($keyName, static::COUNTERS_BIN_TTL);
        });

        $id = $result[0];
        return pack('NN', $id, $time);
    }
    
}
