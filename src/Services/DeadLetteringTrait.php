<?php

namespace Shavshukov\RedisQueue\Services;

use Shavshukov\RedisQueue\Client\Multi;

trait DeadLetteringTrait
{
    
    /**
     * Process message Deal-Letter eXchange (amqp-like)
     * 
     * @param array $sendToDlx
     * @param string $reason
     * @return Generator|null
     */
    protected function processDeadLetters(array $sendToDlx, string $reason)
    {
        $ids = $sendToDlx['id'];
        $queueInfos = $sendToDlx['queueInfo'];
        $dlxQueueInfos = $sendToDlx['dlxQueueInfo'];
        $originalExpiration = $sendToDlx['originalExpiration'];
        $headersJsons = yield $this->messageHeadersSelector->pluck($ids);
        
        $toHeaders = [];
        $toQueueMap = [];
        $requeueTimestampIds = [];
        $requeueIdTimestamps = [];
        
        // TODO: indexed headers
        foreach ($ids as $i => $id) {
            $queueInfo = $queueInfos[$i];
            $dlxQueueInfo = $dlxQueueInfos[$i];
            $timeoutTimestamp = $originalExpiration[$i];
            $headersJson = $headersJsons[$i];
            $headers = $headersJson ? \json_decode($headersJson, true) : [];
            $deadLetterTime = \microtime(true);
            $queue = $queueInfo->getName();
            
            $xDeathRecord = [
                'queue' => $queue,
                'reason' => $reason,
                'time' => $deadLetterTime,
                'count' => 1,
                'original-expiration' => $timeoutTimestamp
            ];

            if (isset($headers['x-death']) && \is_array($headers['x-death'])) {
                $newXDeath = [0];
                foreach ($headers['x-death'] as $entry) {
                    if ($entry['queue'] === $queue && $entry['reason'] === $reason) {
                        $xDeathRecord['count'] += $entry['count'];
                        $xDeathRecord['original-expiration'] = $entry['original-expiration'];
                    } else {
                        $newXDeath[] = $entry;
                    }
                }
                $newXDeath[0] = $xDeathRecord;
            } else {
                $newXDeath = [$xDeathRecord];
            }

            $headers['x-death'] = $newXDeath;

            if (!isset($headers['x-first-death-reason'])) {
                $headers['x-first-death-reason'] = $reason;
            }

            if (!isset($headers['x-first-death-reason'])) {
                $headers['x-first-death-queue'] = $queue;
            }

            $newHeadersJson = \json_encode($headers);
            $toHeaders[] = $id;
            $toHeaders[] = $newHeadersJson;
            $toQueueMap[] = $id;
            $toQueueMap[] = $dlxQueueInfo->getId();
            $requeueTimestampIds[] = \microtime(true);
            $requeueTimestampIds[] = $id;
            $requeueIdTimestamps[] = $id;
            $requeueIdTimestamps[] = \microtime(true);
        }
        
        yield $this->client->multi(function(Multi $multi) use ($ids, $toHeaders, $toQueueMap, $requeueIdTimestamps, $requeueTimestampIds) {
            yield $this->messageHash->bulkUpdate($multi, [
                'headers' => $toHeaders,
                'queueId' => $toQueueMap,
                'timestamp' => $requeueIdTimestamps
            ]);
            yield $this->requeueMessages($multi, $ids, $requeueTimestampIds);
        });
    }
    

}
