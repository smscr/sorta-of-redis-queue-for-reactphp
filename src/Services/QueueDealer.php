<?php

namespace Shavshukov\RedisQueue\Services;

use Exception;
use Generator;
use React\ChildProcess\Process;

use Shavshukov\RedisQueue\{
    Client\Multi,
    Client\Connection,
    Queue\QueueInfoList,
    Queue\QueueKeys,
    Selects\HashColumn,
    Selects\HashObject,
    Objects\MessageHash,
    Services\CountersService
};

class QueueDealer extends AtomicResident
{
    use DeadLetteringTrait;

    const BULK_SIZE = 500;
    
    const DEAD_LETTER_REASON_REJECTED = 'rejected';
    const DEAD_LETTER_REASON_EXPIRED = 'expired';
    const DEAD_LETTER_REASON_MAXLEN = 'maxlen';
    
    const HEARTBEAT_RATE_TTL = 0.05;
    const HEARTBEAT_RATE_DISTRIBUTION = 0.01;
    const HEARTBEAT_RATE_CONSUMER_TIMEOUT = 1.0;
    
    /**
     * @var string|null
     */
    protected $id;
    
    /**
     * @var QueueKeys
     */
    protected $keys;
    
    /**
     * @var int
     */
    protected $offset;
    
    /**
     * @var string
     */
    protected $prefix;
    
    /**
     * @var string
     */
    protected $controlKey;
    
    /**
     * @var QueueInfoList
     */
    protected $queueInfoList;
    
    /**
     * @var MessageHash
     */
    protected $messageHash;
    
    /**
     * @var HashColumn
     */
    protected $messageHeadersSelector;
    
    /**
     * @var HashColumn
     */
    protected $messageTimestampSelector;
    
    /**
     * @var HashObject
     */
    protected $messageProcessSelector;
    
    /**
     * Constructor
     * 
     * @param Connection $connection
     * @param string|null $id
     * @param int $offset
     */
    public function __construct(Connection $connection, string $id = null, int $offset = 0)
    {
        parent::__construct($connection);
        
        $this->id = $id;
        $this->keys = $connection->getQueueKeys();
        $this->messageHash = $this->keys->message;
        $this->messageHeadersSelector = $this->messageHash->createColumnSelector($this->client, 'headers');
        $this->messageTimestampSelector = $this->messageHash->createColumnSelector($this->client, 'timestamp');
        $this->messageProcessSelector = $this->messageHash->createObjectSelector($this->client, ['ttlHeader', 'queueId']);
        $this->offset = $offset;
        $this->prefix = $connection->getPrefix();
        $this->queueInfoList = $connection->getQueueInfoList();
    }

    /**
     * Create child process definition for QueueDealer core
     * 
     * @param string $commandTemplate
     * @param int $offsetMult
     * @return Process
     */
    public function createCoreProcess(string $commandTemplate, int $offsetMult)
    {
        if ($offsetMult < 0) {
            throw new Exception('Offset multiplier must not be negative!');
        }
        
        $hexId = \bin2hex($this->id);
        $offset = $offsetMult * self::BULK_SIZE;
        $commandLine = \sprintf($commandTemplate, \escapeshellarg($hexId), $offset);
        $process = new Process($commandLine, null, null, array());
        $process->on('exit', function ($exitCode, $termSignal) use ($process, $hexId, $offset) {
            echo "QueueDealer core id={$hexId} offset={$offset} exited with code {$exitCode}, restarting...\n";
            $process->start($this->loop);
        });
        
        return $process;
    }

    /**
     * @inheritdoc
     */
    protected function getServiceName(): string
    {
        return 'QueueDealer';
    }
    
    /**
     * @inheritdoc
     */
    protected function register()
    {
        if ($this->id === null) {
            $this->id = yield $this->connection->generateUniqueId(CountersService::TYPE_DEALER);
            yield $this->client->sadd($this->keys->dealersSetKey, $this->id);
        } else {
            $isDealer = yield $this->client->sismember($this->keys->dealersSetKey, $this->id);
            
            if (!$isDealer) {
                throw new Exception("Invalid dealer id: {$this->id}");
            }
        }
        
        $this->controlKey = $this->createControlKey($this->id);
        yield $this->client->set($this->keys->currentDealerIdKey, $this->id);
    }
    
    /**
     * @inheritdoc
     */
    protected function checkRegistration()
    {
        yield $this->client->subscribe($this->keys->channelDealerName);
        $id = yield $this->client->get($this->keys->currentDealerIdKey);
        
        if ($id === $this->id) {
            yield $this->client->publish($this->keys->channelDealerName, $this->id);
            return true;
        }
        
        return false;
    }
    
    /**
     * @inheritdoc
     */
    protected function collectOldResidents()
    {
        $allDealers = yield $this->client->smembers($this->keys->dealersSetKey);
        $yields = [];
        
        foreach ($allDealers as $dealerId) {
            if ($dealerId === $this->id) {
                continue;
            }
            
            $oldControlKey = $this->createControlKey($dealerId);
            $yields[] = $this->massRequeue($oldControlKey, function (Multi $multi) use ($dealerId) {
                yield $multi->srem($this->keys->dealersSetKey, $dealerId);
            });
        }
        
        yield $yields;
    }
    
    /**
     * @return array
     */
    protected function createTimers(): array
    {
        return [
            [self::HEARTBEAT_RATE_TTL, function () {
                // get the oldest message of timed out ones
                $messageIds = yield $this->client->zrangebyscore($this->keys->ttlTimeoutsKey, '-inf', \microtime(true), 'LIMIT', $this->offset, self::BULK_SIZE);

                if ($messageIds) {
                    yield $this->collectTimedOudMessages($messageIds);
                }
            }],
                    
            [self::HEARTBEAT_RATE_DISTRIBUTION, function () {
                // get the oldest message of due to process set
                $bulkResult = yield $this->client->zrangebyscore($this->keys->dueSortedSetKey, '-inf', \microtime(true), 'WITHSCORES', 'LIMIT', $this->offset, self::BULK_SIZE);

                if ($bulkResult) {
                    yield $this->distributeDueSetMessages($bulkResult);
                }
            }],
            
            [self::HEARTBEAT_RATE_CONSUMER_TIMEOUT, function () {
                $deadConsumerIds = yield $this->client->zrangebyscore($this->keys->consumerTimeoutsKey, '-inf', \microtime(true));

                if ($deadConsumerIds) {
                    yield $this->collectDeadConsumers($deadConsumerIds);
                }
            }]
        ];
    }
    
    /**
     * @return array
     */
    protected function createHandlers(): array
    {
        return [
            'message' => [
                function ($channel, $payload) {
                    if ($channel === $this->keys->channelDealerName && $payload !== $this->id) {
                        yield $this->shutdown();
                    }
                }
            ]
        ];
    }
    
    /**
     * Get name of the key used to register the QueueDealer
     * 
     * @param string $id
     * @return string
     */
    protected function createControlKey(string $id): string
    {
        $hexId = \bin2hex($id);
        return "{$this->prefix}.zSets.dealerControl.{$hexId}";
    }
    
    /**
     * Collect messages from ttlTimeoutsKey
     * 
     * @param array $messageIds
     * @return Generator|null
     */
    protected function collectTimedOudMessages(array $messageIds)
    {
        $timestamps = yield $this->messageTimestampSelector->pluck($messageIds);
        yield $this->processMessages($messageIds, $timestamps);
    }
    
    /**
     * Distribute due messages 
     * 
     * @param array $bulkResult
     * @return Generator|null
     */
    protected function distributeDueSetMessages(array $bulkResult)
    {
        $timestamps = [];
        $messageIds = [];
        foreach ($bulkResult as $index => $value) {
            if ($index % 2 === 0) {
                $messageIds[] = $value;
            } else {
                $timestamps[] = $value;
                $nextValue = $bulkResult[$index - 1];
                $bulkResult[$index - 1] = $value;
                $bulkResult[$index] = $nextValue;
            }
        }
        
        // claim it: those messages which are already in control, should not be processed twice
        $claimResult = yield $this->client->multi(function (Multi $multi) use ($messageIds, $bulkResult) {
            yield $multi->zrem($this->keys->dueSortedSetKey, ...$messageIds);
            yield $multi->zadd($this->controlKey, ...$bulkResult);
        });
        
        $isClaimed = $claimResult[1] === \count($messageIds);
        
        if ($isClaimed) {
            yield $this->processMessages($messageIds, $timestamps);
        }
    }

    /**
     * Given dead consumer id's, collect stale messaged from their queues
     * 
     * @param array $deadConsumerIds
     * @return Generator|null
     */
    protected function collectDeadConsumers(array $deadConsumerIds)
    {
        $generators = [];
        foreach ($deadConsumerIds as $id) {
            $hexId = \bin2hex($id);
            $consumerPickupKey = "{$this->prefix}.zSets.consumerPickup.{$hexId}";
            $generators[] = $this->massRequeue($consumerPickupKey, function (Multi $multi) use ($id) {
                yield $multi->zrem($this->keys->consumerTimeoutsKey, $id);
            });
        }
        
        yield $generators;
    }
    
    /**
     * Process message from the queue
     * 
     * @param string[] $messageIds
     * @param float[] $timestamps
     * @return Generator|null
     */
    protected function processMessages(array $messageIds, array $timestamps)
    {
        $result = yield $this->messageProcessSelector->columns($messageIds);
        $messageTtls = $result['ttlHeader'];
        $queueIds = $result['queueId'];
        
        $queueLoadedInfos = yield $this->queueInfoList->getQueueInfos($queueIds);
        list($loadedQueues, $dlxQueueMap) = $queueLoadedInfos;
        
        $queueMessageTtls = [];
        $queueBucketNames = [];
        $queueChannelNames = [];
        
        foreach ($loadedQueues as $id => $queueInfo) {
            $hexId = \bin2hex($id);
            $queueMessageTtls[$id] = $queueInfo->getMessageTtl();
            $queueBucketNames[$id] = "{$this->prefix}.zSets.queue.{$hexId}";
            $queueChannelNames[$id] = "{$this->prefix}.channel.queue.{$hexId}";
        }
        
        $toDelete = [];
        $toConsume = [];
        $fromControl = [];
        $toTtlTimeouts = [];
        $toDeadExchange = [];
        $toActiveQueueIds = [];
        
        foreach ($messageIds as $i => $messageId) {
            // check if the message have correct associated queue, if it don't then we have to drop it :(
            if (!$queueIds[$i] || !$loadedQueues[$queueIds[$i]]) {
                $hex = \bin2hex($messageId);
                $toDelete[] = $messageId;
                echo "Dropping undeliverable message #{$hex}, reason: queue does not exists\n";
                continue;
            }
            
            $queueId = $queueIds[$i];
            $queueInfo = $loadedQueues[$queueId];
            $timestamp = $timestamps[$i];
            $messageTtl = $messageTtls[$i] ?? $queueMessageTtls[$queueId];
            
            // check that the message is not timed out
            if (!$messageTtl || ($timeoutTimestamp = $timestamp + $messageTtl) > \microtime(true)) {
                if ($messageTtl) {
                    // message is actual, lets write to a timeouts set its time of death so we can track it later
                    $toTtlTimeouts[] = $timeoutTimestamp;
                    $toTtlTimeouts[] = $messageId;
                }
                
                // add message to consume queue
                $toActiveQueueIds[$queueId] = true;
                $toConsume[$queueBucketNames[$queueId]][] = $timestamp;
                $toConsume[$queueBucketNames[$queueId]][] = $messageId;
                $fromControl[] = $messageId;
            } else if ($dlxQueueMap[$queueId]) {
                // message is outdated, lets process it as dead letter
                $toDeadExchange['id'][] = $messageId;
                $toDeadExchange['queueInfo'][] = $queueInfo;
                $toDeadExchange['dlxQueueInfo'][] = $loadedQueues[$dlxQueueMap[$queueId]];
                $toDeadExchange['originalExpiration'][] = $timeoutTimestamp;
            } else {
                // if we cannot send the message to any dlx queue, and cannot requeue, can only delete then
                $toDelete[] = $messageId;
            }
        }

        $yields = [];
        $yields[] = $this->client->multi(function (Multi $multi) use ($toTtlTimeouts, $toDelete, $toConsume, $fromControl) {
            yield $this->sendProcessedToKeys($multi, $toConsume, $fromControl, $toTtlTimeouts, $toDelete);
        });
        
        if ($toDeadExchange) {
            $yields[] = $this->processDeadLetters($toDeadExchange, self::DEAD_LETTER_REASON_EXPIRED);
        }
        
        yield $yields;
        
        $yieldsPublish = [];
        if ($toActiveQueueIds) {
            foreach ($toActiveQueueIds as $activeQueueId => $true) {
                $channelName = $queueChannelNames[$activeQueueId];
                // echo "Publish to {$channelName}\n";
                $yieldsPublish[] = $this->client->publish($channelName, '1');
            }
        }
        
        yield $yieldsPublish;
    }
    
    /**
     * Add processed messages to the proper keys for processing
     * 
     * @param Multi $multi
     * @param array $toConsume
     * @param array $fromControl
     * @param array $toTtlTimeouts
     * @param array $toDelete
     */
    protected function sendProcessedToKeys(Multi $multi, array $toConsume, array $fromControl, array $toTtlTimeouts, array $toDelete)
    {
        foreach ($toConsume as $bucketKey => $toBucket) {
            yield $multi->zadd($bucketKey, ...$toBucket);
        }
        
        if ($fromControl) {
            yield $multi->zrem($this->controlKey, ...$fromControl);
        }
        
        if ($toTtlTimeouts) {
            yield $multi->zadd($this->keys->ttlTimeoutsKey, ...$toTtlTimeouts);
        }

        if ($toDelete) {
            yield $this->deleteMessages($multi, $toDelete);
        }
    }
    
    /**
     * Mass re-queue messages that was left by a previous QueueDealer
     * 
     * @param string $keyName
     * @param callable $append
     * @return Generator|null
     * @throws Exception
     */
    protected function massRequeue(string $keyName, callable $append = null)
    {
        $bulkResult = yield $this->client->zrangebyscore($keyName, '-inf', '+inf', 'WITHSCORES');
        if (!$bulkResult) {
            if ($append) {
                yield $this->client->multi(function (Multi $multi) use ($append) {
                    yield $append($multi);
                });
            }
            return;
        }
        
        $messageIds = [];
        foreach ($bulkResult as $index => $value) {
            if ($index % 2 === 0) {
                $messageIds[] = $value;
                continue;
            }
            
            $nextValue = $bulkResult[$index - 1];
            $bulkResult[$index - 1] = $value;
            $bulkResult[$index] = $nextValue;
        }
        
        $count = \count($messageIds);
        echo "Collect: {$count} from key {$keyName}\n";
        $result = yield $this->client->multi(function (Multi $multi) use ($messageIds, $bulkResult, $keyName, $append) {
            yield $this->requeueMessages($multi, $messageIds, $bulkResult);
            yield $multi->del($keyName);
            
            if ($append) {
                yield $append($multi);
            }
        });
        
        if (!end($result)) {
            $print = print_r($result, true);
            throw new Exception("Mass requeue have failed, response:\n{$print}");
        }
    }
    
    /**
     * @param Multi $multi
     * @param string[] $id
     * @return Generator|null
     */
    protected function deleteMessages(Multi $multi, array $id)
    {
        yield $multi->zrem($this->controlKey, ...$id);
        yield $multi->zrem($this->keys->ttlTimeoutsKey, ...$id);
        yield $multi->zrem($this->keys->dueSortedSetKey, ...$id);
        yield $this->messageHash->deleteEntries($multi, $id);
    }
    
    /**
     * @param Multi $multi
     * @param string[] $id
     * @param array[float, string, ...] $timestampId
     * @return Generator|null
     */
    protected function requeueMessages(Multi $multi, array $id, array $timestampId)
    {
        yield $multi->zrem($this->controlKey, ...$id);
        yield $multi->zrem($this->keys->ttlTimeoutsKey, ...$id);
        yield $multi->zadd($this->keys->dueSortedSetKey, ...$timestampId);
        yield $this->messageHash->bulkDelete($multi, ['isClaimed' => $id]);
    }

}
