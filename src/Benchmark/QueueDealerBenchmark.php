<?php

namespace Shavshukov\RedisQueue\Benchmark;

use Generator;
use Shavshukov\RedisQueue\Services\QueueDealer;

class QueueDealerBenchmark extends QueueDealer
{
    
    const HEARTBEAT_RATE_BENCHMARK = 60.0;
    
    /**
     * @var array
     */
    protected $counts = [];

    /**
     * @var array
     */
    protected $timeDiffs = [];

    /**
     * @var array
     */
    protected $speeds = [];
    
    /**
     * @inheritdoc
     */
    protected function processMessages(array $messageIds, array $timestamps)
    {
        $start = \microtime(true);
        yield parent::processMessages($messageIds, $timestamps);
        yield $this->benchmark($start, \count($messageIds));
    }
    
    /**
     * @return array
     */
    protected function createTimers(): array
    {
        $timers = parent::createTimers();
        $timers[] =  [self::HEARTBEAT_RATE_BENCHMARK, function () {
            yield $this->printBenchmark();
        }];
        return $timers;
    }
    
    /**
     * @param float $start
     * @param int $count
     */
    protected function benchmark(float $start, int $count)
    {
        $time = \microtime(true);
        $timeDiff = ($time - $start);
        $perSecond = $count * (1.0 / $timeDiff);
        $this->counts[] = $count;
        $this->timeDiffs[] = $timeDiff;
        $this->speeds[] = $perSecond;
    }

    /**
     * @return Generator
     */
    protected function printBenchmark()
    {
        if (!$this->counts) {
            return;
        }

        $c = $this->counts;
        $sp = $this->speeds;
        $tD = $this->timeDiffs;
        $this->counts = [];
        $this->speeds = [];
        $this->timeDiffs = [];

        $totalC = \array_sum($c);
        $totalT = \array_sum($tD);
        $totalPs = $totalC / $totalT;

        $cCount = \count($c);
        $spCount = \count($sp);
        $tdCount = \count($tD);
        $averageC = $cCount ? ($totalC / $cCount) : 0;
        $averageSp = $spCount ? (\array_sum($sp) / $spCount) : 0;
        $averageTd = $tdCount ? ($totalT / $tdCount) : 0;

        $date = \date(DATE_RFC850);
        $report = "[{$date} - benchmark] Avg: {$averageC} per {$averageTd}, {$averageSp}/s, Total: {$totalC} per {$totalT}, {$totalPs}/sn";
        $promise = $this->client->publish($this->keys->channelDealerBenchmarkName, $report);
        yield $promise;
    }
}
