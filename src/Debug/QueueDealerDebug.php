<?php

namespace Shavshukov\RedisQueue\Debug;

use React\EventLoop\TimerInterface;
use Shavshukov\RedisQueue\{
    Client\Multi,
    Client\Connection,
    Services\QueueDealer
};

class QueueDealerDebug extends QueueDealer
{
    
    /**
     * @var array
     */
    protected $stats;
    
    /**
     * @var array
     */
    protected $unique = [
        'timed_out' => [],
        'due_set' => [],
    ];
    
    /**
     * @var TimerInterface
     */
    protected $printTimer = null;
    
    /**
     * Constructor
     * 
     * @param Connection $connection
     * @param string|null $id
     * @param int $offset
     */
    public function __construct(Connection $connection, $id = null, int $offset = 0)
    {
        parent::__construct($connection, $id, $offset);
        $this->stats = $this->getStatsDefaults();
    }
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        yield parent::init();
        $this->printStats();
    }

    /**
     * @inheritdoc
     */
    protected function collectTimedOudMessages(array $messageIds)
    {
        yield parent::collectTimedOudMessages($messageIds);
        yield $this->historyLog($messageIds, 'collected by timeout');
        
        $unique = 0;
        foreach ($messageIds as $id) {
            if (!isset($this->unique['timed_out'][$id])) {
                $this->unique['timed_out'][$id] = 1;
                ++$unique;
            }
        }
        
        $this->stats['timed_out'] += \count($messageIds);
        $this->stats['timed_out_unique'] += $unique;
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function distributeDueSetMessages(array $bulkResult)
    {
        yield parent::distributeDueSetMessages($bulkResult);
        
        $count = 0;
        $unique = 0;
        foreach ($bulkResult as $index => $id) {
            if ($index % 2 === 0) {
                ++$count;
                if (!isset($this->unique['due_set'][$id])) {
                    $this->unique['due_set'][$id] = 1;
                    ++$unique;
                }
            }
        }

        $this->stats['due_set'] += $count;
        $this->stats['due_set_unique'] += $unique;
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function collectDeadConsumers(array $deadConsumerIds)
    {
        yield parent::collectDeadConsumers($deadConsumerIds);
        $this->stats['dead_consumers'] += \count($deadConsumerIds);
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function sendProcessedToKeys(Multi $multi, array $toConsume, array $fromControl, array $toTtlTimeouts, array $toDelete)
    {
        yield parent::sendProcessedToKeys($multi, $toConsume, $fromControl, $toTtlTimeouts, $toDelete);
        yield $this->historyLog($fromControl, 'sent to consumer');
        $this->stats['sent_to_consume'] += \count($toConsume);
        $this->stats['sent_to_ttl_timeouts'] += \count($toTtlTimeouts);
        $this->stats['sent_to_delete'] += \count($toDelete);
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function processDeadLetters(array $sendToDlx, string $reason)
    {
        yield parent::processDeadLetters($sendToDlx, $reason);
        yield $this->historyLog($sendToDlx['id'], 'dead letter');
        
        $this->stats['sent_to_dlx'] += \count($sendToDlx['id']);
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function deleteMessages(Multi $multi, array $ids)
    {
        yield parent::deleteMessages($multi, $ids);
        yield $this->historyLog($ids, 'delete');
        
        $this->stats['deleted'] += \count($ids);
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function requeueMessages(Multi $multi, array $ids, array $timestampId)
    {
        yield parent::requeueMessages($multi, $ids, $timestampId);
        yield $this->historyLog($ids, 'requeue');
        
        $this->stats['requeued'] += \count($ids);
        $this->printStats();
    }
    
    /**
     * @inheritdoc
     */
    protected function processMessages(array $messageIds, array $timestamps)
    {
        yield parent::processMessages($messageIds, $timestamps);
        $count = \count($messageIds);
        $this->stats['processed'] += $count;
        $this->printStats();
    }
    
    /**
     * Log into history
     * 
     * @param array $ids
     * @param string $whatHappened
     */
    protected function historyLog(array $ids, string $whatHappened)
    {
        foreach ($ids as $id) {
            $time = \microtime(true);
            $hexId = \bin2hex($id);
            yield $this->client->append("{$this->prefix}.history.{$hexId}", "{$time}: {$whatHappened}\n");
            yield $this->client->expire("{$this->prefix}.history.{$hexId}", 3600);
        }
    }
    
    /**
     * Get default values for statistics reset 
     * 
     * @return array
     */
    protected function getStatsDefaults()
    {
        return [
            'start' => \microtime(true),
            'timed_out' => 0,
            'timed_out_unique' => 0,
            'due_set' => 0,
            'due_set_unique' => 0,
            'dead_consumers' => 0,
            'sent_to_consume' => 0,
            'sent_to_ttl_timeouts' => 0,
            'sent_to_delete' => 0,
            'sent_to_dlx' => 0,
            'deleted' => 0,
            'requeued' => 0,
            'processed' => 0
        ];
    }
    
    /**
     * Print collected statistics
     * 
     * @return null
     */
    protected function printStats()
    {
        if ($this->printTimer) {
            return;
        }
        
        $this->printTimer = $this->loop->addTimer(5.0, function () {
            $this->printTimer = null;
            $diff = \microtime(true) - $this->stats['start'];
            echo "\n{$diff} seconds\n\n";
            \print_r($this->stats);
            $this->stats = $this->getStatsDefaults();
        });
    }

}
