<?php

namespace Shavshukov\RedisQueue\Debug;

use Generator;
use React\Promise\PromiseInterface;
use function Shavshukov\Coroutine\coroutine;

use Shavshukov\RedisQueue\{
    Client\Multi,
    Services\QueueSubscriber
};

class QueueSubscriberDebug extends QueueSubscriber
{
    /**
     * @inheritdoc
     */
    protected function claimMessages(array &$messageIds, array &$timestamps, array &$bulkResult, array $claimingMessage)
    {
        $notClaimedIds = yield parent::claimMessages($messageIds, $timestamps, $bulkResult, $claimingMessage);
        
        yield $this->historyLog($messageIds, 'claimed');
        yield $this->historyLog($notClaimedIds, 'falied to claim');
        
        return $notClaimedIds;
    }
    
    /**
     * Accept message (delete it)
     * 
     * @param string $messageId
     * @return PromiseInterface
     */
    public function accept(string $messageId)
    {
        $id = \hex2bin($messageId);
        return coroutine(function() use ($id) {
            yield $this->client->multi(function (Multi $multi) use ($id) {
                yield $this->deleteMessages($multi, [$id]);
                yield $this->historyLog([$id], 'deleted after accept');
            });
        })->otherwise(function ($error) {
            $this->connection->reportError($error, false);
        });
    }
    
    /**
     * @param Multi $multi
     * @param string[] $id
     * @param array[float, string, ...] $timestampId
     * @return Generator|null
     */
    protected function requeueMessages(Multi $multi, array $id, array $timestampId)
    {
        yield parent::requeueMessages($multi, $id, $timestampId);
        yield $this->historyLog($id, 'requeued by consumer');
    }
    
    /**
     * Log into history
     * 
     * @param array $ids
     * @param string $whatHappened
     */
    protected function historyLog(array $ids, string $whatHappened)
    {
        foreach ($ids as $id) {
            $time = \microtime(true);
            $hexId = \bin2hex($id);
            yield $this->client->append("{$this->prefix}.history.{$hexId}", "{$time}: {$whatHappened}\n");
            yield $this->client->expire("{$this->prefix}.history.{$hexId}", 3600);
        }
    }
    

}
