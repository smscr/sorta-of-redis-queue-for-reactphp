<?php

namespace Shavshukov\RedisQueue\Selects;

use Exception;
use Generator;
use Shavshukov\RedisQueue\Client\ClientPipeline;

class HashObject
{
    
    /**
     * @var array [
     *     string $fieldName => string $hashKey,
     *     ...
     * ]
     */
    protected $hashKeys;
    
    /**
     * @var string[]
     */
    protected $fields;
    
    /**
     * @var ClientPipeline
     */
    protected $client;

    /**
     * Constructor
     * 
     * @param ClientPipeline $client
     * @param array $hashKeys [
     *     string $fieldName => string $hashKey,
     *     ...
     * ]
     */
    public function __construct(ClientPipeline $client, array $hashKeys)
    {
        $this->hashKeys = $hashKeys;
        $this->fields = \array_keys($hashKeys);
        $this->client = $client;
    }
    
    /**
     * Get columns for specified fields of keys
     * 
     * @param array $keys
     * @return Generator|array [...$values]
     * @throws Exception
     */
    public function columns(array $keys)
    {
        if (!$keys) {
            return [];
        }

        $yields = [];
        foreach ($this->hashKeys as $fieldName => $hashKey) {
            $yields[$fieldName] = $this->client->hmget($hashKey, ...$keys);
        }

        $result = yield $yields;
        return $result;
    }
    
    /**
     * Select fields of specified keys and return result entries by key
     * 
     * @param array $keys
     * @return Generator|array [
     *     string $key => [
     *         string $fieldName => mixed $value,
     *         ...
     *     ],
     *     ...
     * ]
     */
    public function getAll(array $keys)
    {
        $result = yield $this->columns($keys);
        
        $resultEntriesByKey = [];
        foreach ($this->fields as $i => $fieldName) {
            foreach ($keys as $j => $key) {
                $resultEntriesByKey[$key][$fieldName] = $result[$i][$j];
            }
        }
        
        return $resultEntriesByKey;
    }
    
    
}
