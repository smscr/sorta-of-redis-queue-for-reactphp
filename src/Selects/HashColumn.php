<?php

namespace Shavshukov\RedisQueue\Selects;

use Exception;
use Generator;
use Shavshukov\RedisQueue\Client\{Multi, ClientPipeline};

class HashColumn
{
    
    /**
     * @var string
     */
    protected $hashKey;
    
    /**
     * @var ClientPipeline
     */
    protected $client;

    /**
     * Constructor
     * 
     * @param ClientPipeline $client
     * @param string $hashKey
     */
    public function __construct(ClientPipeline $client, string $hashKey)
    {
        $this->hashKey = $hashKey;
        $this->client = $client;
    }
    
    /**
     * Get one specific column for keys
     * 
     * @param array $keys
     * @param Multi|null $multi
     * @return Generator|array [...$values]|null
     * @throws Exception
     */
    public function pluck(array $keys, Multi $multi = null)
    {
        if (!$keys) {
            return [];
        }

        if ($multi) {
            yield $multi->hmget($this->hashKey, ...$keys);
            return;
        }
        
        $result = yield $this->client->hmget($this->hashKey, ...$keys);
        return $result;
    }
    
    /**
     * Select fields of specified keys and return result entries by key
     * 
     * @param array $keys
     * @return Generator|array [
     *     string $key => mixed $value,
     * ]|null
     */
    public function get(array $keys)
    {
        if (!$keys) {
            return [];
        }

        $result = yield $this->client->hmget($this->hashKey, ...$keys);
        $resultsByKey = [];

        foreach ($keys as $i => $key) {
            $resultsByKey[$key] = $result[$i];
        }

        return $resultsByKey;
    }
    
}
