<?php

namespace Shavshukov\RedisQueue\Client;

use React\Promise;
use React\EventLoop\LoopInterface;
use function Shavshukov\Coroutine\coroutine;
use Shavshukov\RedisQueue\Interfaces\ErrorHandlerInterface;

class ConnectionFactory
{
    /**
     * Connection options
     * 
     * @var array
     */
    protected $options;
    
    /**
     * ErrorHandlerInterface instance
     *
     * @var ErrorHandlerInterface 
     */
    protected $errorHandler;
    
    /**
     * Constructor
     * 
     * @param array $connectionOptions [
     *     'password' => string,
     *     'host' => string,
     *     'port' => int,
     *     'db' => int,
     *     'prefix' => string|null
     * ]
     */
    public function __construct(array $connectionOptions, ErrorHandlerInterface $errorHandler = null)
    {
        $this->options = $connectionOptions;
        $this->errorHandler = $errorHandler;
    }
    
    /**
     * {@inheritDoc}
     */
    public function createConnection(LoopInterface $loop): Promise\PromiseInterface
    {
        if ($this->options['password']) {
            $encodedPassword = urlencode($this->options['password']);
            $auth = ":{$encodedPassword}@";
        } else {
            $auth = '';
        }
        
        // [redis[s]://][:auth@]host[:port][/db]
        $redisString = "redis://{$auth}{$this->options['host']}:{$this->options['port']}/{$this->options['db']}";
        $client = new ClientPipeline($loop, $redisString);
        
        if ($this->errorHandler) {
            $client->setErrorHandler($this->errorHandler);
        }
        
        return coroutine(function() use ($client, $loop) {
            yield $client->clientAvailable();
            $connection = new Connection($client, $loop, $this->options['prefix'] ?? null);
            
            if ($this->errorHandler) {
                $connection->setErrorHandler($this->errorHandler);
            }
            
            return $connection;
        });
    }
}
