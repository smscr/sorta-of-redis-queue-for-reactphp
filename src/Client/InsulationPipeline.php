<?php

namespace Shavshukov\RedisQueue\Client;

use Throwable;
use React\Promise;
use React\EventLoop\LoopInterface;
use function Shavshukov\Coroutine\coroutine;

class InsulationPipeline
{
    /**
     * @var LoopInterface
     */
    protected $loop;
    
    /**
     * @var \React\Promise\ExtendedPromiseInterface|\React\Promise\Promise\CancellablePromiseInterface
     */
    protected $promise;
    
    /**
     * @var callable
     */
    protected $canceller;
    
    /**
     * @var callable
     */
    protected $onRejection;
    
    /**
     * Constructor
     * 
     * @param LoopInterface $loop
     * @param callable $canceller
     * @param callable $onRejection
     */
    public function __construct(LoopInterface $loop, callable $canceller = null, callable $onRejection = null)
    {
        $this->loop = $loop;
        $this->promise = Promise\resolve();
        $this->canceller = $canceller;
        $this->onRejection = $onRejection;
    }
    
    /**
     * Add a runnable to InsulationPipeline
     * 
     * @param mixed $runnable
     * @return \React\Promise\PromiseInterface
     */
    public function add($runnable)
    {
        $deferred = new Promise\Deferred($this->canceller);
        $promise = $deferred->promise();
        $prevPromise = $this->promise;
        $onRejection = $this->onRejection;
        $this->promise = $promise;
        
        $this->loop->futureTick(static function() use ($deferred, $prevPromise, $runnable, $onRejection) {
            coroutine(static function () use ($deferred, $prevPromise, $runnable, $onRejection) {
                try {
                    $prevResult = yield $prevPromise;
                    $result = yield $runnable($prevResult, $deferred);
                    $deferred->resolve($result);
                } catch (Throwable $error) {
                    if ($onRejection === null) {
                        $deferred->reject($error);
                        return;
                    }
                    
                    try {
                        $result = yield $onRejection($error);
                        $deferred->resolve($result);
                    }  catch (Throwable $error) {
                        $deferred->reject($error);
                    }
                }
            });
        });
        
        return $promise;
    }
    
    /**
     * @return null
     */
    public function cancel()
    {
        $this->promise->cancel();
    }
    
}
