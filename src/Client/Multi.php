<?php

namespace Shavshukov\RedisQueue\Client;

use Throwable;
use Exception;
use Generator;
use Clue\React\Redis\Client;

class Multi
{
    const STATE_INIT = 0;
    const STATE_STARTED = 1;
    const STATE_ENDED = 2;
    
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var ClientPipeline 
     */
    protected $pipeline;
    
    /**
     * @var array|null
     */
    protected $exceptClients;
    
    /**
     * @var int
     */
    protected $state = self::STATE_INIT;
    
    /**
     * Constructor
     * 
     * @param ClientPipeline $pipeline
     * @param Client $client
     * @param array $exceptClients
     */
    public function __construct(ClientPipeline $pipeline, Client $client, array $exceptClients = null)
    {
        $this->client = $client;
        $this->pipeline = $pipeline;
        $this->exceptClients = $exceptClients;
    }
    
    /**
     * Transaction
     * 
     * @param callable $callback
     * @param array|null $exceptClients
     * @return Generator|mixed
     */
    public function multi(callable $callback, array $exceptClients = null)
    {
        if ($exceptClients === null) {
            $exceptClients = $this->exceptClients;
        } else {
            $exceptClients = \array_merge($exceptClients, $this->exceptClients);
        }
        
        return $this->pipeline->multi($callback, $exceptClients);
    }
    
    /**
     * Execute multi
     * 
     * @param callable $callback
     * @return Generator|mixed
     * @throws Exception
     */
    public function exec(callable $callback)
    {
        if ($this->state > self::STATE_INIT) {
            throw new Exception('This Multi has already started!');
        }
        
        $this->state = self::STATE_STARTED;
        yield $this->client->multi();
        try {
            yield $callback($this);
            $result = yield $this->client->exec();
            $this->state = self::STATE_ENDED;
        } catch (Throwable $e) {
            try {
                yield $this->client->discard();
                $this->state = self::STATE_ENDED;
            } catch (Throwable $e2) {
                $this->pipeline->reportError($e2, false);
            }
            
            throw $e;
        }
        
        return $result;
    }
    
    /**
     * Magic calls into the client
     * 
     * @param string $name
     * @param array $arguments
     * @return \React\Promise\PromiseInterface<mixed>
     */
    public function __call($name, $arguments)
    {
        if ($this->state > self::STATE_ENDED) {
            throw new Exception('This Multi has already ended!');
        }
        
        return $this->client->$name(...$arguments);
    }
    
}
