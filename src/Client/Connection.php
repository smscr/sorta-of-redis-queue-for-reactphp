<?php

namespace Shavshukov\RedisQueue\Client;

use Throwable;
use Exception;
use Generator;
use React\Promise;
use React\EventLoop\LoopInterface;
use function Shavshukov\Coroutine\coroutine;

use Shavshukov\RedisQueue\{
    Interfaces\SubscriptionInterface,
    Interfaces\QueueableMessageInterface,
    Queue\QueueInfoList,
    Queue\QueueKeys,
    Services\CountersService,
    Services\QueueSubscriber,
    Errors\ErrorHandlingTrait
};

class Connection
{
    use ErrorHandlingTrait;
    
    /**
     * @var ClientPipeline
     */
    protected $client;
    
    /**
     * @var LoopInterface
     */
    protected $loop;
    
    /**
     * Prefix for all queues
     *
     * @var string
     */
    protected $prefix;
    
    /**
     * All subscribers by their tags
     *
     * @var QueueSubscriber[] 
     */
    protected $subscribers = [];
    
    /**
     * @var CountersService
     */
    protected $counters;
    
    /**
     * @var QueueInfoList
     */
    protected $queueInfoList;
    
    /**
     * @var QueueKeys
     */
    protected $keys;
    
    /**
     * Constructor
     * 
     * @param ClientPipeline $client
     * @param LoopInterface $loop
     * @param string $prefix
     */
    public function __construct(ClientPipeline $client, LoopInterface $loop, string $prefix)
    {
        $this->loop = $loop;
        $this->client = $client;
        $this->prefix = $prefix;
        $this->keys = new QueueKeys($prefix);
        $this->counters = new CountersService($this);
        $this->queueInfoList = new QueueInfoList($this);
    }
    
    /**
     * Generate an entity id with a very strong uniqueness
     * 
     * @param string $type
     * @return Generator|string
     */
    public function generateUniqueId(string $type)
    {
        return $this->counters->generateUniqueId($type);
    }
    
    public function multiPublish(array $messages)
    {
        return coroutine(function () use ($messages) {
            $multiInsert = [];
            $toDue = [];

            foreach ($messages as $message) {
                $messageId = yield $this->generateUniqueId(CountersService::TYPE_MESSAGE);
                $multiInsert[$messageId] = $messageInsert = yield $this->getMessageInsert($message);
                $toDue[] = $messageInsert['timestamp'];
                $toDue[] = $messageId;
            }
            
            yield $this->client->multi(function ($multi) use ($multiInsert, $toDue) {
                yield $this->keys->message->insert($multi, $multiInsert);
                yield $multi->zadd($this->keys->dueSortedSetKey, ...$toDue);
            });
        })->otherwise(function (Throwable $e) {
            $this->reportError($e, false);
            return Promise\reject($e);
        });
    }
    
    /**
     * {@inheritDoc}
     */
    public function publish(QueueableMessageInterface $message)
    {
        return coroutine(function () use ($message) {
            $messageId = yield $this->generateUniqueId(CountersService::TYPE_MESSAGE);
            $messageInsert = yield $this->getMessageInsert($message);
            
            yield $this->client->multi(function ($multi) use ($messageId, $messageInsert) {
                $timestamp = $messageInsert['timestamp'];
                yield $this->keys->message->insert($multi, [$messageId => $messageInsert]);
                yield $multi->zadd($this->keys->dueSortedSetKey, $timestamp, $messageId);
//                $hexId = \bin2hex($messageId);
//                echo "[{$time}] Inserted: {$hexId}\n";
//                yield $multi->append("{$this->prefix}.history.{$hexId}", "{$time}: inserted\n");
//                yield $multi->expire("{$this->prefix}.history.{$hexId}", 3600);
            });

            return $messageId;
        })->otherwise(function (Throwable $e) {
            $this->reportError($e, false);
            return Promise\reject($e);
        });
    }
    
    /**
     * Subscribe to the queue
     * 
     * @param SubscriptionInterface $subscription
     * @return Promise\PromiseInterface
     */
    public function subscribe(SubscriptionInterface $subscription)
    {
        return coroutine(function () use ($subscription) {
            $queueInfo = yield $this->queueInfoList->getQueueInfoByName($subscription->getQueueName());
            if (!$queueInfo) {
                throw new Exception("Queue {$queueName} does not exists!");
            }
            
            $subscriber = new QueueSubscriber($this, $subscription, $queueInfo);
            yield $subscriber->init();
            $hexId = \bin2hex($subscriber->getId());
            $this->subscribers[$hexId] = $subscriber;
            return $hexId;
        })->otherwise(function (Throwable $e) {
            $this->reportError($e, false);
            return Promise\reject($e);
        });
    }
    
    /**
     * Un-subscribe from the queue
     * 
     * @param string $consumerTag
     * @return Promise\PromiseInterface
     */
    public function unsubscribe(string $consumerTag)
    {
        if (isset($this->subscribers[$consumerTag])) {
            return coroutine(function () use ($consumerTag) {
                echo "Unsubscribe: {$consumerTag}\n";
                yield $this->subscribers[$consumerTag]->shutdown();
                unset($this->subscribers[$consumerTag]);
            })->otherwise(function (Throwable $e) {
                $this->reportError($e, false);
                return Promise\reject($e);
            });
        }
        
        return Promise\resolve();
    }
    
    /**
     * Get prefix for queues
     * 
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }
    
    /**
     * Get QueueKeys instance
     * 
     * @return QueueKeys
     */
    public function getQueueKeys(): QueueKeys
    {
        return $this->keys;
    }
    
    /**
     * Get QueueInfoList instance
     * 
     * @return QueueInfoList
     */
    public function getQueueInfoList(): QueueInfoList
    {
        return $this->queueInfoList;
    }
    
    /**
     * Get ClientPipeline instance
     * 
     * @return ClientPipeline
     */
    public function getClientPipeline(): ClientPipeline
    {
        return $this->client;
    }
    
    /**
     * Get React loop instance
     * 
     * @return LoopInterface
     */
    public function getLoop(): LoopInterface
    {
        return $this->loop;
    }
    
    /**
     * @param QueueableMessageInterface $message
     * @return Generator|array
     * @throws Exception
     */
    protected function getMessageInsert(QueueableMessageInterface $message)
    {
        $queueName = $message->getQueueName();
        $contents = $message->getContents();
        $headers = $message->getHeadersArray();
        $ttlHeader = $headers['x-message-ttl'] ?? null;
        $headersJson = \json_encode($headers);

        if ($ttlHeader !== null) {
            if ($ttlHeader > 0) {
                $ttlHeader = (float) $ttlHeader / 1000;
            } else {
                $ttlHeader = 0;
            }
        }

        $queueInfo = yield $this->queueInfoList->getQueueInfoByName($queueName);
        if (!$queueInfo) {
            throw new Exception("Queue {$queueName} does not exists!");
        }

        $queueId = $queueInfo->getId();

        return [
            'body' => $contents,
            'headers' => $headersJson,
            'timestamp' => \microtime(true),
            'ttlHeader' => $ttlHeader,
            'queueId' => $queueId,
        ];
    }
}
