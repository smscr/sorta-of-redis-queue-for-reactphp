<?php

namespace Shavshukov\RedisQueue\Client;

use Generator;
use Clue\React\Redis\Client;
use Evenement\EventEmitterInterface;

class Channels implements EventEmitterInterface
{
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var array
     */
    protected $subscriptions = [];
    
    /**
     * @var array
     */
    protected $psubscriptions = [];
    
    /**
     * @var array
     */
    protected $listeners = [];
    
    /**
     * @var array
     */
    protected $onceListeners = [];
    
    /**
     * Constructor
     * 
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    
    /**
     * Replace Client instance and re-subscribe all subscriptions, place all handlers
     * 
     * @param Client $client
     * @return Generator|null
     */
    public function replaceClient(Client $client)
    {
        $this->client = $client;
        
        foreach ($this->listeners as $event => $listeners) {
            foreach ($listeners as $listener) {
                $this->client->on($event, $listener);
                // echo "Restored listener {$event}\n";
            }
        }
        
        foreach ($this->onceListeners as $event => $listeners) {
            foreach ($listeners as $listener) {
                $this->client->once($event, $listener);
                // echo "Restored onceListener {$event}\n";
            }
        }
        
        if (\count($this->subscriptions)) {
            yield $this->client->subscribe(...\array_keys($this->subscriptions));
            // echo "Restored subscriptions\n";
        }
        
        if (\count($this->psubscriptions)) {
            yield $this->client->psubscribe(...\array_keys($this->psubscriptions));
            // echo "Restored psubscriptions\n";
        }
    }
    
    /**
     * @param array $arguments
     * @return Generator|mixed
     */
    public function subscribe(...$arguments)
    {
        $result = yield $this->client->subscribe(...$arguments);
        
        foreach ($arguments as $channelName) {
            $this->subscriptions[$channelName] = true;
            // echo "subscribe {$channelName}\n";
        }
        
        return $result;
    }
    
    /**
     * @param array $arguments
     * @return Generator|mixed
     */
    public function unsubscribe(...$arguments)
    {
        $result = yield $this->client->unsubscribe(...$arguments);
        
        foreach ($arguments as $channelName) {
            // echo "unsubscribe {$channelName}\n";
            unset($this->subscriptions[$channelName]);
        }
        
        return $result;
    }
    
    /**
     * @param array $arguments
     * @return Generator|mixed
     */
    public function psubscribe(...$arguments)
    {
        $result = yield $this->client->psubscribe(...$arguments);
        
        foreach ($arguments as $channelPattern) {
            // echo "psubscribe {$channelPattern}\n";
            $this->psubscriptions[$channelPattern] = true;
        }
        
        return $result;
    }
    
    /**
     * @param array $arguments
     * @return Generator|mixed
     */
    public function punsubscribe(...$arguments)
    {
        $result = yield $this->client->punsubscribe(...$arguments);
        
        foreach ($arguments as $channelPattern) {
            // echo "punsubscribe {$channelPattern}\n";
            unset($this->psubscriptions[$channelPattern]);
        }
        
        return $result;
    }
    
    /**
     * @param string $event
     * @param callable $listener
     * @return mixed
     */
    public function on($event, callable $listener)
    {
        $result = $this->client->on($event, $listener);
        // echo "Added event {$event}\n";
        $this->listeners[$event][] = $listener;
        return $result;
    }

    /**
     * @param string $event
     * @param array $arguments
     * @return mixed
     */
    public function emit($event, array $arguments = [])
    {
        $result = $this->client->emit($event, $arguments);
        unset($this->onceListeners[$event]);
        return $result;
    }

    /**
     * @param string $event
     * @return mixed
     */
    public function listeners($event = null)
    {
        return $this->client->listeners($event);
    }

    /**
     * @param string $event
     * @param callable $listener
     * @return mixed
     */
    public function once($event, callable $listener)
    {
        $result = $this->client->once($event, $listener);
        // echo "Added event once {$event}\n";
        $this->onceListeners[$event][] = $listener;
        return $result;
    }

    /**
     * @param string $event
     * @return mixed
     */
    public function removeAllListeners($event = null)
    {
        $result = $this->client->removeAllListeners($event);
        
        if ($event !== null) {
            unset($this->listeners[$event]);
        } else {
            $this->listeners = [];
        }

        if ($event !== null) {
            unset($this->onceListeners[$event]);
        } else {
            $this->onceListeners = [];
        }
        
        return $result;
    }

    /**
     * @param string $event
     * @param callable $listener
     * @return mixed
     */
    public function removeListener($event, callable $listener)
    {
        $result = $this->client->removeListener($event, $listener);
        if (isset($this->listeners[$event])) {
            $index = \array_search($listener, $this->listeners[$event], true);
            if (false !== $index) {
                unset($this->listeners[$event][$index]);
                if (\count($this->listeners[$event]) === 0) {
                    unset($this->listeners[$event]);
                }
            }
        }

        if (isset($this->onceListeners[$event])) {
            $index = \array_search($listener, $this->onceListeners[$event], true);
            if (false !== $index) {
                unset($this->onceListeners[$event][$index]);
                if (\count($this->onceListeners[$event]) === 0) {
                    unset($this->onceListeners[$event]);
                }
            }
        }
        return $result;
    }

}
