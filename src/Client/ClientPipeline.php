<?php

namespace Shavshukov\RedisQueue\Client;

use Exception;
use Generator;
use Throwable;
use Clue\React\Redis\Client;
use Clue\React\Redis\Factory;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use React\Promise;
use React\Promise\PromiseInterface;
use Evenement\EventEmitterInterface;
use function Shavshukov\Coroutine\coroutine;

use Shavshukov\RedisQueue\Errors\ErrorHandlingTrait;

class ClientPipeline implements EventEmitterInterface
{
    use ErrorHandlingTrait;
    
    const CLIENT_INACTIVITY = 60;
    const MAX_CLIENTS_DEFAULT = 1000;
    const REQUEST_TIMEOUT_SECONDS = 5;
    
    const CHANNELS_CALLS = [
        'subscribe' => true,
        'psubscribe' => true,
        'unsubscribe' => true,
        'punsubscribe' => true,
    ];
    
    const CHANNELS_EVENTS = [
        'subscribe' => true,
        'psubscribe' => true,
        'unsubscribe' => true,
        'punsubscribe' => true,
        'message' => true,
        'pmessage' => true,
    ];
    
    /**
     * @var LoopInterface
     */
    protected $loop;
    
    /**
     * @var Factory
     */
    protected $factory;
    
    /**
     * @var string
     */
    protected $redisString;

    /**
     * @var int
     */
    protected $maxClientsNum;
    
    /**
     * @var int
     */
    protected $currentClientsNum = 0;
    
    /**
     * @var int
     */
    protected $maxClientId = 0;
    
    /**
     * @var int[]
     */
    protected $lastActivity = [];
    
    /**
     * @var TimerInterface[]
     */
    protected $inactivityTimers = [];
    
    /**
     * @var Client[]
     */
    protected $availableClients = [];

    /**
     * @var Client[]
     */
    protected $busyClients = [];
    
    /**
     * @var InsulationPipeline|array [
     *     int $clientId => InsulationPipeline $pipeline
     * ]
     */
    protected $pipelines = [];

    /**
     * @var int[] [
     *     int $clientId => int $load
     * ]
     */
    protected $pipelineLoad = [];
    
    /**
     * @var bool
     */
    protected $end = false;
    
    /**
     * @var Channels|null
     */
    protected $channels;
    
    /**
     * @var Client|null
     */
    protected $channelsClient;
    
    /**
     * Constructor
     * 
     * @param LoopInterface $loop
     * @param string $redisString
     */
    public function __construct(LoopInterface $loop, string $redisString, int $maxClientsNum = self::MAX_CLIENTS_DEFAULT)
    {
        if ($maxClientsNum <= 0) {
            throw new Exception('Maximum number of clients must not be zero or negative');
        }
        
        $this->loop = $loop;
        $this->factory = new Factory($loop);
        $this->redisString = $redisString;
        $this->maxClientsNum = $maxClientsNum;
    }
    
    /**
     * Wait until at least one client is available
     * 
     * @return Generator|void
     */
    public function clientAvailable()
    {
        if (empty($this->availableClients)) {
            yield $this->makeAvailableClient();
        }
    }
    
    /**
     * Get client for subscribing to channels
     * 
     * @return Generator|Channels
     * @throws Exception
     */
    public function getChannelsInstance()
    {
        if (!$this->channelsClient) {
            if ($this->end) {
                throw new Exception('This ClientPipeline is ended');
            }
            
            $this->channelsClient = yield $this->factory->createClient($this->redisString);
            $this->channels = new Channels($this->channelsClient);
            
            $this->channels->on('error', function ($error) {
                $this->reportErrort($error, false);
                $this->channelsClient->close();
                $this->replaceChannelsClient()->otherwise(function () {
                    echo "Subscriptions closed\n";
                });
            });
            
            $this->channels->on('close', function () {
                $this->replaceChannelsClient()->otherwise(function () {
                    echo "Subscriptions closed\n";
                });
            });
        }
        
        return $this->channels;
    }
    
    /**
     * @return Generator|null
     */
    protected function replaceChannelsClient()
    {
        return coroutine(function() {
            if ($this->end) {
                throw new Exception('This ClientPipeline is ended');
            }

            while (true) {
                try {
                    $this->channelsClient = yield $this->factory->createClient($this->redisString);
                    break;
                } catch (Throwable $error) {
                    $this->reportErrort($error, false);
                }
            }

            yield $this->channels->replaceClient($this->channelsClient);
            echo "Channes client replaced\n";
        });
    }
    
    /**
     * Transaction
     * 
     * @param callable $callback
     * @param array|null $exceptClients
     * @return Generator|mixed
     */
    public function multi(callable $callback, array $exceptClients = null)
    {
        return $this->takeClient(function (Client $client, int $id) use ($callback, $exceptClients) {
            // for multi, all future nestings must except that client which have multi
            if ($exceptClients === null) {
                $exceptClients = [$id => true];
            } else {
                $exceptClients[] = $id;
            }
            
            return (new Multi($this, $client, $exceptClients))->exec($callback);
        }, $exceptClients);
    }
    
    /**
     * End all connections once all pending requests have been replied to
     */
    public function end()
    {
        $this->end = true;
        if ($this->channelsClient) {
            $this->channelsClient->end();
            $this->channelsClient = null;
        }
        
        $promises = [];
        
        foreach ($this->pipelines as $pipeline) {
            $promises[] = $pipeline->add(function () use ($pipeline) {
                $pipeline->cancel();
            });
        }
        
        yield $promises;
    }

    /**
     * Close connection immediately
     */
    public function close()
    {
        $this->end = true;
        if ($this->channelsClient) {
            $this->channelsClient->close();
            $this->channelsClient = null;
        }
        
        foreach ($this->pipelines as $id => $pipeline) {
            $pipeline->cancel();
            $this->removeClient($id, true);
        }
    }
    
    /**
     * Magic calls into the client
     * 
     * @param string $name
     * @param array $arguments
     * @return PromiseInterface
     */
    public function __call($name, $arguments)
    {
        if (isset(self::CHANNELS_CALLS[$name])) {
            return coroutine($this->channelsCall($name, $arguments));
        }
        
        return $this->takeClient(static function (Client $client) use ($name, $arguments) {
            $result = yield $client->$name(...$arguments);
            return $result;
        });
    }
    
    /**
     * Take client for a walk of some work
     * 
     * @param callable $consumer
     * @param array|null $exceptClients
     * @return Generator|mixed
     */
    protected function takeClient(callable $consumer, array $exceptClients = null)
    {
        $id = yield $this->takeMostAvailableClientId($exceptClients);
        
        ++$this->pipelineLoad[$id];
        $result = yield Promise\Timer\timeout($this->pipelines[$id]->add(function () use ($id, $consumer, $exceptClients) {
            if (!isset($this->busyClients[$id])) {
                // the client "died" doing its previous work, and we need to try another one
                return $this->takeClient($consumer, $exceptClients);
            }
            
            $client = $this->busyClients[$id];
            
            try {
                $result = yield $consumer($client, $id);
                
                if (isset($this->pipelineLoad[$id])) {
                    if (--$this->pipelineLoad[$id] === 0) {
                        unset($this->busyClients[$id]);
                        $this->availableClients[$id] = $client;
                        $this->lastActivity[$id] = time();
                    }
                }
                
                return $result;
            } catch (Throwable $error) {
                $this->removeClient($id);
                throw $error;
            }
        }), self::REQUEST_TIMEOUT_SECONDS, $this->loop);
        
        return $result;
    }
    
    /**
     * Take the client which is least loaded for a walk of work
     * 
     * @param array|null $exceptClients
     * @return Generator|id
     */
    protected function takeMostAvailableClientId(array $exceptClients = null)
    {
        if (empty($this->availableClients)) {
            $id = yield $this->makeAvailableClient(true, $exceptClients);
        } else {
            $client = end($this->availableClients);
            $id = key($this->availableClients);
            unset($this->availableClients[$id]);
            $this->busyClients[$id] = $client;
        }
        
        return $id;
    }
    
    /**
     * Find the client which is least loaded, or create one, possibly made busy
     * 
     * @param bool $busy
     * @param array|null $exceptClients
     * @return Generator|id|null
     */
    protected function makeAvailableClient(bool $busy = false, array $exceptClients = null)
    {
        if ($this->currentClientsNum >= $this->maxClientsNum) {
            if ($exceptClients === null) {
                $idByLoad = \array_flip($this->pipelineLoad);
            } else {
                $idByLoad = \array_flip(\array_diff_key($this->pipelineLoad, $exceptClients));
                if (!\count($idByLoad)) {
                    throw new Exception(
                        "The maximum number of clients are reached, and all available clients ({$this->currentClientsNum}) are nested"
                    );
                }
            }

            \ksort($idByLoad);
            $result = \reset($idByLoad);
        } else {
            ++$this->currentClientsNum;
            $result = yield $this->addClient($busy);
        }
        
        return $result;
    }
    
    /**
     * Add new client to available clients
     * 
     * @param bool $busy
     * @return Generator|null|int
     */
    protected function addClient(bool $busy)
    {
        if ($this->end) {
            throw new Exception('This ClientPipeline is ended');
        }
        
        $newId = ++$this->maxClientId;
        echo "Created client {$newId}\n";
        
        try {
            $client = yield $this->factory->createClient($this->redisString);
        } catch (Throwable $error) {
            echo "Cannot create client!\n";
            $this->reportErrort($error, false);
            --$this->currentClientsNum;
            return;
        }
        
        $this->pipelines[$newId] = $this->createClientPipeline($newId);
        $this->pipelineLoad[$newId] = 0;
        $this->setInactivityTimer($newId);
        
        if ($busy) {
            $this->busyClients[$newId] = $client;
            return $newId;
        } else {
            $this->availableClients[$newId] = $client;
            $this->lastActivity[$newId] = \time();
        }
    }
    
    /**
     * Set time that will automatically remove excessive clients
     * 
     * @param int $id
     */
    protected function setInactivityTimer(int $id)
    {
        if (isset($this->inactivityTimers[$id])) {
            $this->loop->cancelTimer($this->inactivityTimers[$id]);
        }
        
        $this->inactivityTimers[$id] = $this->loop->addPeriodicTimer(self::CLIENT_INACTIVITY, function () use ($id) {
            if (!isset($this->lastActivity[$id])) {
                $this->loop->cancelTimer($this->inactivityTimers[$id]);
                return;
            }
            
            if ($this->currentClientsNum < 2 || !isset($this->availableClients[$id])) {
                return;
            }

            if ($this->lastActivity[$id] < (\time() - self::CLIENT_INACTIVITY)) {
                echo "Remove by inactivity: {$id}\n";
                $this->removeClient($id);
            }
        });
    }
    
    /**
     * Create a client insulation pipeline which ensures that one client won't process commands not in the order of
     * their issue and will replace the client by another client if some of the previous commands on that connection
     * have failed, possibly discrediting the connection on which the some of commands have failed
     * 
     * @param int $id
     * @return InsulationPipeline
     */
    protected function createClientPipeline(int $id)
    {
        return new InsulationPipeline($this->loop, function () use ($id) {
            echo "Pipeline cancelled\n";
            $this->removeClient($id);
        });
    }
    
    /**
     * Close a client
     * 
     * @param int $id
     * @param bool $close
     */
    protected function removeClient(int $id, bool $close = false)
    {
        echo "Removing client {$id}\n";
        if (isset($this->busyClients[$id])) {
            $client = $this->busyClients[$id];
            unset($this->busyClients[$id]);
            
            if ($close) {
                $client->close();
            } else {
                $client->end();
            }
            
            --$this->currentClientsNum;
        }
        
        if (isset($this->availableClients[$id])) {
            $client = $this->availableClients[$id];
            unset($this->availableClients[$id]);
            
            if ($close) {
                $client->close();
            } else {
                $client->end();
            }
            
            --$this->currentClientsNum;
        }
        
        if (isset($this->inactivityTimers[$id])) {
            $this->loop->cancelTimer($this->inactivityTimers[$id]);
            unset($this->inactivityTimers[$id]);
        }
        
        unset($this->pipelines[$id]);
        unset($this->pipelineLoad[$id]);
        unset($this->lastActivity[$id]);
    }
    
    /**
     * @param string $event
     * @param callable $listener
     * @return mixed
     */
    public function on($event, callable $listener)
    {
        $channels = yield $this->eventCall($event);
        return $channels->on($event, $listener);
    }

    /**
     * @param string $event
     * @param array $arguments
     * @return mixed
     */
    public function emit($event, array $arguments = [])
    {
        $channels = yield $this->eventCall($event);
        return $channels->emit($event, $arguments);
    }

    /**
     * @param string $event
     * @return mixed
     */
    public function listeners($event = null)
    {
        $channels = yield $this->eventCall($event);
        return $channels->listeners($event);
    }

    /**
     * @param string $event
     * @param callable $listener
     * @return mixed
     */
    public function once($event, callable $listener)
    {
        $channels = yield $this->eventCall($event);
        return $channels->once($event, $listener);
    }

    /**
     * @param string $event
     * @return mixed
     */
    public function removeAllListeners($event = null)
    {
        $channels = yield $this->eventCall($event);
        return $channels->removeAllListeners($event);
    }

    /**
     * @param string $event
     * @param callable $listener
     * @return mixed
     */
    public function removeListener($event, callable $listener)
    {
        $channels = yield $this->eventCall($event);
        return $channels->removeListener($event, $listener);
    }
    
    /**
     * @param string $name
     * @param array $arguments
     * @return Generator|mixed
     */
    protected function channelsCall($name, $arguments)
    {
        $channels = yield $this->getChannelsInstance();
        $result = $channels->$name(...$arguments);
        return $result;
    }
    
    /**
     * @param string|null $event
     * @return mixed
     * @throws Exception
     */
    protected function eventCall($event = null)
    {
        if ($event !== null && !isset(self::CHANNELS_EVENTS[$event])) {
            throw new Exception('ClientPipeline instance only routes events directly to Channels instance (PUB/SUB events)');
        }
        
        $channels = yield $this->getChannelsInstance();
        return $channels;
    }

}
