<?php

namespace Shavshukov\RedisQueue\Interfaces;

interface SubscriptionInterface
{
    public function getQueueName(): string;
    public function getMessageFactory(): MessageFactoryInterface;
    public function receive($message);    
}
