<?php

namespace Shavshukov\RedisQueue\Interfaces;

interface RejectableMessageInterface
{
    /**
     * Get additional headers that come with the message
     * 
     * @return array
     */
    public function getHeadersArray(): array;
    
    /**
     * Get name of the queue this message came from
     * 
     * @return string
     */
    public function getQueueName(): string;
    
    /**
     * Get id of the message
     * 
     * @return string
     */
    public function getId(): string;
}
