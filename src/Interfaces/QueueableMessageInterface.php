<?php

namespace Shavshukov\RedisQueue\Interfaces;

interface QueueableMessageInterface
{
    /**
     * Get contents of the message
     * 
     * @return string
     */
    public function getContents(): string;
    
    /**
     * Get additional headers that come with the message
     * 
     * @return array
     */
    public function getHeadersArray(): array;
    
    /**
     * Get name of the queue this message came from
     * 
     * @return string
     */
    public function getQueueName(): string;
}
