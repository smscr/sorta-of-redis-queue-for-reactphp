<?php

namespace Shavshukov\RedisQueue\Interfaces;

use Throwable;

interface ErrorHandlerInterface
{
    public function report(Throwable $error, bool $fatal);
}
