<?php

namespace Shavshukov\RedisQueue\Interfaces;

use Shavshukov\RedisQueue\Services\QueueSubscriber;

interface MessageFactoryInterface
{
    /**
     * @param QueueSubscriber $subsriber
     * @param array $data [
     *     'id' => string,
     *     'body' => string,
     *     'headers' => array,
     *     'queueName' => string,
     * ]
     * @return mixed
     */
    public function createMessage(QueueSubscriber $subsriber, array $data);
}
