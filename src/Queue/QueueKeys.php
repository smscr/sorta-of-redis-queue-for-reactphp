<?php

namespace Shavshukov\RedisQueue\Queue;

use Shavshukov\RedisQueue\Objects\MessageHash;

class QueueKeys
{
    
    public $dealersSetKey;
    
    public $currentDealerIdKey;
    
    public $ttlTimeoutsKey;
    public $dueSortedSetKey;
    public $consumerTimeoutsKey;
    
    public $queueIndexKey;
    public $queueInfoListKey;
    
    public $channelDealerName;
    public $channelDealerBenchmarkName;
    
    /**
     * @var MessageHash
     */
    public $message;
    
    protected $prefix;
    
    /**
     * Constructor
     * 
     * @param string $prefix
     */
    public function __construct(string $prefix)
    {
        $this->prefix = $prefix;
        
        $this->dealersSetKey = "{$prefix}.sets.dealersSet";
        
        $this->currentDealerIdKey = "{$prefix}.values.currentDealerId";
        
        $this->ttlTimeoutsKey = "{$prefix}.zSets.ttiTimeouts";
        $this->dueSortedSetKey = "{$prefix}.zSets.dueSchedule";
        $this->consumerTimeoutsKey = "{$prefix}.zSets.consumerTimeouts";
        
        $this->queueIndexKey = "{$prefix}.hashes.queueIndex";
        $this->queueInfoListKey = "{$prefix}.hashes.queueInfo";

        
        $this->channelDealerName = "{$prefix}.channels.dealer";
        $this->channelDealerBenchmarkName = "{$prefix}.channels.dealer-benchmark";
        
        $this->message = new MessageHash($this);
    }
    
    /**
     * Get full key name of the hash for field of the object
     * 
     * @param string $objectName
     * @param string $fieldName
     * @return string
     */
    public function getHashField(string $objectName, string $fieldName): string
    {
        return "{$this->prefix}.hashes.{$objectName}:{$fieldName}";
    }
    
}
