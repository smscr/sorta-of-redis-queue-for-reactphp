<?php

namespace Shavshukov\RedisQueue\Queue;

use Exception;

class QueueInfo
{
    
    /**
     * @var string|null
     */
    protected $channelName;
    
    /**
     * @var array
     */
    protected $data;
    
    /**
     * Constructor
     * 
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    
    /**
     * Update
     * 
     * @param array $data
     * @return null
     * @throws Exception
     */
    public function update(array $data)
    {
        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
        }
    }
    
    /**
     * @throws Exception
     * @return null
     */
    public function validate()
    {
        if (($notFoundFields = array_diff(['id', 'name'], array_keys($this->data)))) {
            $implodeNotFound = implode(', ', $notFoundFields);
            throw new Exception("Queue info mandatory fields not found: {$implodeNotFound}");
        }
    }        
    
    /**
     * @return array
     */
    public function source(): array
    {
        return $this->data;
    }
    
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->data['id'];
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->data['name'];
    }
    
    /**
     * @return string|null
     */
    public function getDlxQueueName(): ?string
    {
        return $this->data['dlxQueueName'] ?? null;
    }
    
    /**
     * @return int|null
     */
    public function getMessageTtl(): ?int
    {
        return $this->data['messageTtl'] ?? null;
    }
    
}
