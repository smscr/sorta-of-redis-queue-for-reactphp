<?php

namespace Shavshukov\RedisQueue\Queue;

use Generator;
use Shavshukov\RedisQueue\{
    Client\Multi,
    Client\Connection,
    Services\CountersService
};

class QueueInfoList
{
    
    /**
     * Connection instance
     *
     * @var Connection
     */
    protected $connection;
    
    /**
     * QueueKeys instance
     * 
     * @var QueueKeys
     */
    protected $keys;
    
    /**
     * ClientPipeline instance
     * 
     * @var ClientPipeline
     */
    protected $client;
    
    /**
     * List of all currently loaded queues by id
     *
     * @var array [
     *     string $queueId => QueueInfo|null $info
     * ]
     */
    protected $loadedQueues = [];
    
    /**
     * List of all currently loaded queue id's by name
     *
     * @var array [
     *     string $queueName => string|null $queueId
     * ]
     */
    protected $loadedQueueIds = [];
    
    /**
     * List of dlx queue id for currently loaded queues by id
     *
     * @var array [
     *     string $queueId => string|null $dlxQueueId
     * ]
     */
    protected $dlxQueueMap = [];
    
    /**
     * Constructor
     * 
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->keys = $connection->getQueueKeys();
        $this->client = $connection->getClientPipeline();
    }
    
    /**
     * Ensure all specified queue id's are loaded and return loaded queue ids
     * 
     * @param array $queueIds
     * @return array [
     *     array $loadedQueues,
     *     array $loadedQueueIds,
     *     array $dlxQueueMap
     * ]
     */
    public function getQueueInfos(array $queueIds)
    {
        foreach ($queueIds as $id) {
            if ($id && !array_key_exists($id, $this->loadedQueues)) {
                yield $this->loadQueue($id);
            }
        }
        
        return [
            $this->loadedQueues,
            $this->dlxQueueMap
        ];
    }
    
    /**
     * Set QueueInfo data
     * 
     * @param string $queueName
     * @param array $data
     */
    public function setQueueInfo(string $queueName, array $data)
    {
        $currentQueueInfo = yield $this->getQueueInfoByName($queueName);
        
        if ($currentQueueInfo) {
            $queueInfo = new QueueInfo($currentQueueInfo->source());
            $queueInfo->update($data);
            $id = $queueInfo->getId();
        } else {
            $queueInfo = new QueueInfo($data);
            $id = yield $this->connection->generateUniqueId(CountersService::TYPE_QUEUE);
            $queueInfo->update(['id' => $id, 'name' => $queueName]);
        }
        
        $queueInfo->validate();
        $source = $queueInfo->source();
        $source['id'] = \bin2hex($id);
        $json = \json_encode($source);
        yield $this->client->multi(function (Multi $multi) use ($id, $queueName, $json) {
            yield $multi->hset($this->keys->queueIndexKey, $queueName, $id);
            yield $multi->hset($this->keys->queueInfoListKey, $id, $json);
        });
        
        $this->loadedQueues[$id] = $queueInfo;
        $this->loadedQueueIds[$queueName] = $id;
    }
    
    /**
     * Get QueueInfo instance by a queue name
     * 
     * @param string $queueName
     * @param bool $optional
     * @return Generator|QueueInfo|null
     */
    public function getQueueInfoByName(string $queueName)
    {
        if (!\array_key_exists($queueName, $this->loadedQueueIds)) {
            $result = yield $this->client->hget($this->keys->queueIndexKey, $queueName);
            $this->loadedQueueIds[$queueName] = $result;
            return $result ? $this->getQueueInfoById($result) : null;
        }
        
        $id = $this->loadedQueueIds[$queueName];
        return $id ? $this->loadedQueues[$id] : null;
    }
    
    /**
     * Get QueueInfo instance by a queue id
     * 
     * @param string $queueId
     * @param bool $optional
     * @return Generator|QueueInfo|null
     */
    public function getQueueInfoById(string $queueId)
    {
        if (!\array_key_exists($queueId, $this->loadedQueues)) {
            yield $this->loadQueue($queueId);
        }
        
        return $this->loadedQueues[$queueId];
    }
    
    /**
     * Load queue with specified id
     * 
     * @param string $queueId
     */
    protected function loadQueue(string $queueId)
    {
        $hex = \bin2hex($queueId);
        // echo "Loading queue: {$hex}...\n";
        
        $result = yield $this->client->hget($this->keys->queueInfoListKey, $queueId);

        if ($result) {
            $queueInfo = $this->decodeQueueInfo($result);
            $this->loadedQueues[$queueId] = $queueInfo;
            if (($dlxQueueName = $queueInfo->getDlxQueueName())) {
                $dlxQueueInfo = yield $this->getQueueInfoByName($dlxQueueName);
                $this->dlxQueueMap[$queueId] = $dlxQueueInfo ? $dlxQueueInfo->getId() : null;
            } else {
                $this->dlxQueueMap[$queueId] =  null;
            }
            // echo "Found queue: {$hex}\n";
        } else {
            $this->loadedQueues[$queueId] = null;
            $this->dlxQueueMap[$queueId] = null;
            // echo "Not found queue: {$hex}\n";
        }
    }
    
    /**
     * Decode QueueInfo from json
     * 
     * @param string $queueInfoJson
     * @return QueueInfo
     */
    protected function decodeQueueInfo(string $queueInfoJson): QueueInfo
    {
        $source = \json_decode($queueInfoJson, true);
        $source['id'] = \hex2bin($source['id']);
        $queueInfo = new QueueInfo($source);
        $queueInfo->validate();
        $this->loadedQueueIds[$source['name']] = $source['id'];
        return $queueInfo;
    }
    
}
